#include "stdafx.h"
#include "CppUnitTest.h"
#include "Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(PlayerTest)
	{
	public:

		TEST_METHOD(playerConstructor)
		{
			Player player;

			Assert::AreEqual(player.getName(), std::string("Undefined"));
			Assert::AreEqual(player.getAge(), -1);
			Assert::IsTrue(player.getCartState() &&
				player.getLampState() &&
				player.getPickaxeState() &&
				player.getType() == Enums::PlayerType::Saboteur);
			Assert::AreEqual((int)player.getGold(), 0);
		}

		TEST_METHOD(playerConstructorWithGivenParameters)
		{
			Player player;
			player.setName("Stefan");
			player.setAge(19);
			player.setType(Enums::PlayerType::Saboteur);
			Assert::AreEqual(player.getName(), std::string("Stefan"));
			Assert::AreEqual(player.getAge(), 19);
			Assert::IsTrue(player.getCartState() &&
				player.getLampState() &&
				player.getPickaxeState() &&
				player.getType() == Enums::PlayerType::Saboteur);
			Assert::AreEqual((int)player.getGold(), 0);

		}



	};
}