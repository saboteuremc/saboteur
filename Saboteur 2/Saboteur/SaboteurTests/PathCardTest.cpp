#include "stdafx.h"
#include "CppUnitTest.h"
#include "PathCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(PathCardTest)
	{
	public:

		TEST_METHOD(ConstructorPathCard)
		{
			PathCard p;
			Assert::IsTrue(p.getPathToCenter() == false &&
				p.getPathToEast() == false &&
				p.getPathToNorth() == false &&
				p.getPathToSouth() == false &&
				p.getPathToWest() == false &&
				p.getType() == Enums::PathCardType::Undefined);
		}

		TEST_METHOD(IsGoldCard)
		{
			PathCard p;
			p.setToGold(true);
			Assert::IsTrue(p.isFinalGoldCard());
		}

		TEST_METHOD(ConstructorPathCardWithGivenParameters)
		{
			PathCard p(Enums::PathCardType::NSWEC, false);
			Assert::IsFalse(p.getPathToCenter() == false &&
				p.getPathToEast() == false &&
				p.getPathToNorth() == false &&
				p.getPathToSouth() == false &&
				p.getPathToWest() == false &&
				p.getType() == Enums::PathCardType::NSWEC &&
				p.isFinalGoldCard() == true);
		}

		TEST_METHOD(PathCardRotateFunction)
		{
			PathCard p(Enums::PathCardType::NC, false);
			PathCard rotatedP = p;
			rotatedP.rotate();
			Assert::IsTrue(rotatedP.getPathToSouth() && rotatedP.getPathToCenter());

		}



	};
}