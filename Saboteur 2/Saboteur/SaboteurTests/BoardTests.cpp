#include "stdafx.h"
#include "CppUnitTest.h"
#include "Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{		
	TEST_CLASS(BoardTests)
	{
	
		public:

			TEST_METHOD(BoardConstructor)
			{
				Board board;
				Assert::IsTrue(board[Board::Position(0, 0)].value().isFinalGoldCard() ||
					board[Board::Position(0, 2)].value().isFinalGoldCard() ||
					board[Board::Position(0, 4)].value().isFinalGoldCard() &&
					board[Board::Position(8, 2)].value().getType() == Enums::PathCardType::NSWEC);

			}

			TEST_METHOD(PathReachGold)
			{
				Board board;
				board[Board::Position(7, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(6, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(5, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(4, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(3, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(2, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 0)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 1)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 3)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 4)] = Enums::PathCardType::NSWEC;

				Assert::IsTrue(board.pathReachedGold(board, Enums::PlayerType::BlueTeam));
			}

			TEST_METHOD(PathDoesntReachGold)
			{
				Board board;
				board[Board::Position(7, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(6, 2)] = Enums::PathCardType::NSWE;
				board[Board::Position(5, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(4, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(3, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(2, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 2)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 0)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 1)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 3)] = Enums::PathCardType::NSWEC;
				board[Board::Position(1, 4)] = Enums::PathCardType::NSWEC;

				Assert::IsFalse(board.pathReachedGold(board, Enums::PlayerType::BlueTeam));
			}

			TEST_METHOD(inBoundPosition)
			{
				Board board;
				Board::Position p(4, 4);
				Assert::IsTrue(board.inBound(p));
			}

			TEST_METHOD(notInBoundPosition)
			{
				Board board;
				Board::Position p(70, 70);
				Assert::IsFalse(board.inBound(p));
			}

			TEST_METHOD(squaredBracketGetterOperator)
			{
				Board board;
				Board::Position position(3, 3);
				Assert::IsFalse(board[position].has_value());
			}

			TEST_METHOD(boardException)
			{
				Board board;
				Board::Position position(70, 70);
				auto boardException = [board, position]() {
					return board[position];
				};
				Assert::ExpectException<const char*>(boardException);
			}

	};
}