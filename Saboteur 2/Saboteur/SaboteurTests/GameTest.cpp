#include "stdafx.h"
#include "CppUnitTest.h"
#include "Game.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(GameTest)
	{
	public:

		TEST_METHOD(canPutCardOnBoard)
		{
			Game game;
			Board board;
			Board::Position position(7, 3);
			PathCard * path = new PathCard(Enums::PathCardType::NSWEC);
			Assert::IsFalse(game.canPutCardOnBoard(board, position, path));
		}

		TEST_METHOD(initPathCards)
		{
			Game game;
			int index = 0;
			//game.initPathCardDeck();
			game.initPathCards(index);

			for (int i = 0; i < 40; i++)
			{
				Assert::IsTrue(game.getPathCardDeck().getDeck()[i].has_value());
			}
		}

		TEST_METHOD(initActionCards)
		{
			Game game;
			int index = 0;
			game.initActionCards(index);

			for (int i = 0; i < 27; i++)
			{
				Assert::IsTrue(game.getPathCardDeck().getDeck()[i].has_value());
			}
		}


		TEST_METHOD(initGoldCardDeck)
		{
			Game game;
			game.initGoldCardDeck();

			Assert::IsTrue(game.getGoldCardDeck().size() == 28);
		}

		TEST_METHOD(distrinutePathCards)
		{
			int numberOfPlayers = 5;
			int index = 0;
			Game game;
			game.setNumberOfPlayers(numberOfPlayers);
			game.distributePathCards();

			for (auto player : game.getPlayers())
			{
				Assert::IsTrue(player->getCards().getDeck().size() == 6);
			}
		}

		TEST_METHOD(distributeDwarfCards)
		{
			Game game;
			game.setNumberOfPlayers(6);
			for (int i = 0; i < 6; i++)
			{
				game.getPlayers().push_back(new Player);
			}
			game.initDwarfCardDeck();
			game.distributeDwarfCards();
			Assert::IsTrue(std::any_of(game.getPlayers().begin(), game.getPlayers().end(),
				[](Player* player) {return player->getType() == Enums::PlayerType::Saboteur; }));

			Assert::IsTrue(std::any_of(game.getPlayers().begin(), game.getPlayers().end(),
				[](Player* player) {return player->getType() == Enums::PlayerType::BlueTeam || player->getType() == Enums::PlayerType::GreenTeam; }));
		}

		TEST_METHOD(allPLayersHaveEmptyHands)
		{
			Game game;
			Assert::IsTrue(game.allPlayersHaveEmptyHands());
			int index = 0;

			game.setNumberOfPlayers(6);
			for (int i = 0; i < 6; i++)
			{
				game.getPlayers().push_back(new Player);
			}

			int numberOfPlayers = 6;
			game.distributePathCard(numberOfPlayers, game.getPlayers()[0], index);

		}

	};
}