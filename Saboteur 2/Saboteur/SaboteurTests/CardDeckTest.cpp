#include "stdafx.h"
#include "CppUnitTest.h"
#include "CardDeck.h"
#include "PathCard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(DeckCardTest)
	{
	public:
		TEST_METHOD(shuffleDeck)
		{
			CardDeck deck;
			deck.setSize(3);
			deck[0] = new PathCard(Enums::PathCardType::NC);
			deck[1] = new PathCard(Enums::PathCardType::NSEC);
			deck[2] = new PathCard(Enums::PathCardType::NSC);
			CardDeck deck2;
			deck2 = deck;
			deck2.shuffle();

			Assert::IsFalse(deck.getDeck() == deck2.getDeck());
		}


		TEST_METHOD(deckException)
		{
			CardDeck deck;
			int index = 50;
			auto deckException = [deck, index]() {
				return deck[index];
			};
			Assert::ExpectException<const char*>(deckException);
		}



	};
}