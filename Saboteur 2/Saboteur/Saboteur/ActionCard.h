#pragma once
#include "Card.h"
#include "Player.h"
#include "Board.h"
#include<vector>

class ActionCard : public Card
{
public:
	ActionCard(const Enums::ActionCardType& type = Enums::ActionCardType::Undefined);
	ActionCard(ActionCard&& otherCard);
	ActionCard(const ActionCard& otherCard);
	~ActionCard() = default;

	/* Getter */
	Enums::ActionCardType getType();

	/* Setter */
	void setType(Enums::ActionCardType type);

	/* Operators */
	friend std::istream& operator>>(std::istream& is, ActionCard& card);
	friend std::ostream& operator<<(std::ostream& os, const ActionCard& card);
	ActionCard& operator=(const ActionCard& otherCard);
	ActionCard& operator=(ActionCard&& otherCard);

	/* Useful Functions */
	void Print() override;

	void executeAction(Player* playerSender, Player* playerAffected, Board& board, Board::Position& position);

private:
	void executeBrokenCard(Player* player, Board& board, Board::Position& position);
	void executeRepairCard(Player* player, Board& board, Board::Position& position);
	void executeMapCard(Player* player, Board& board, Board::Position& position);
	void executeRockFallCard(Player* player, Board& board, Board::Position& position);
	
	/* Expansion */
	void executeTheft(Player* player);
	void executeHandsOff(Player* player);
	void executeSwapYourHand(Player* playerSender, Player* playerAffected);
	void executeInspection(Player* player);
	void executeSwapYourHats(Player* playerSender, Player* playerAffected);
	void executeTrapped(Player* player);
	void executeFreeAtLeast(Player* player);

	void revealDestination(Board::Position & position, const Player* player, Board& board);
	void removePath(Board::Position& position, Board& board);

private:
	Enums::ActionCardType type;
};

