#include "PathCard.h"


PathCard::PathCard(const Enums::PathCardType & type, const bool& isGold) :
	type(type), isGold(isGold)
{
	this->pathToCenter = false;
	this->pathToEast = false;
	this->pathToNorth = false;
	this->pathToSouth = false;
	this->pathToWest = false;
	this->isRotate = false;
	this->haveCrystal = false;
	this->isAnEntry = false;
	setPossiblePaths(type);
	setPossibleConnections(type);
	setIfHaveCrystal(type);
	setIfIsEntry(type);

	visitedNSC = false;
	visitedWEC = false;

	visitedNWC = false;
	visitedSEC = false;

	visitedNEC = false;
	visitedSWC = false;
}

PathCard::PathCard(PathCard && otherCard)
{
	*this = std::move(otherCard);
}

PathCard::PathCard(const PathCard & otherCard)
{
	*this = otherCard;
}

Enums::PathCardType PathCard::getType()
{
	return this->type;
}

bool PathCard::getPathToNorth()
{
	return this->pathToNorth;
}

bool PathCard::getPathToSouth()
{
	return this->pathToSouth;
}

bool PathCard::getPathToWest()
{
	return this->pathToWest;
}

bool PathCard::getPathToEast()
{
	return this->pathToEast;
}

bool PathCard::getPathToCenter()
{
	return this->pathToCenter;
}

bool PathCard::isFinalGoldCard()
{
	return isGold;
}

bool PathCard::isRotated()
{
	return this->isRotate;
}

bool PathCard::getHaveCrystal()
{
	return this->haveCrystal;
}

bool PathCard::getIfIsEntry()
{
	return this->isAnEntry;
}

bool PathCard::getVisitedNSC()
{
	return this->visitedNSC;
}

bool PathCard::getVisitedWEC()
{
	return this->visitedWEC;
}

bool PathCard::getVisitedNWC()
{
	return this->visitedNWC;
}

bool PathCard::getVisitedSEC()
{
	return this->visitedSEC;
}

bool PathCard::getVisitedNEC()
{
	return this->visitedNEC;
}

bool PathCard::getVisitedSWC()
{
	return this->visitedSWC;
}

Enums::PlayerType PathCard::getTeamDoor()
{
	if ( this->type == Enums::PathCardType::NSC_B ||
		this->type == Enums::PathCardType::NWC_B || 
		this->type == Enums::PathCardType::WEC_B)
	{
		return Enums::PlayerType::BlueTeam;
	}
	else if (this->type == Enums::PathCardType::NSC_G ||
		this->type == Enums::PathCardType::SWC_G ||
		this->type == Enums::PathCardType::WEC_S_G)
	{
		return Enums::PlayerType::GreenTeam;
	}
	return Enums::PlayerType::Undefinded;
}

PathCard::Coordonates PathCard::getCanConnectTo()
{
	return this->canConnectTo;
}

void PathCard::setType(Enums::PathCardType type)
{
	this->type = type;
	setPossiblePaths(type);
	setPossibleConnections(type);
	setIfHaveCrystal(type);
	setIfIsEntry(type);
}

void PathCard::setPathToNorth(const bool & isPath)
{
	this->pathToNorth = isPath;
}

void PathCard::setPathToSouth(const bool & isPath)
{
	this->pathToSouth = isPath;
}

void PathCard::setPathToWest(const bool & isPath)
{
	this->pathToWest = isPath;
}

void PathCard::setPathToEast(const bool & isPath)
{
	this->pathToEast = isPath;
}

void PathCard::setPathToCenter(const bool & isPath)
{
	this->pathToCenter = isPath;
}

void PathCard::setToGold(const bool & isGold)
{
	this->isGold = isGold;
}

void PathCard::setRotate(const bool & rotate)
{
	if (this->isRotate != rotate)
	{
		this->rotate();
		this->isRotate = rotate;
	}
}

void PathCard::setIsEntry(const bool & isEntry)
{
	this->isAnEntry = isEntry;
}

void PathCard::setVisitedNSC(const bool & NSC)
{
	this->visitedNSC = NSC;
}

void PathCard::setVisitedWEC(const bool & WEC)
{
	this->visitedWEC = WEC;
}

void PathCard::setVisitedNWC(const bool & NWC)
{
	this->visitedNWC = NWC;
}

void PathCard::setVisitedSEC(const bool & SEC)
{
	this->visitedSEC = SEC;
}

void PathCard::setVisitedNEC(const bool & NEC)
{
	this->visitedNEC = NEC;
}

void PathCard::setVisitedSWC(const bool & SWC)
{
	this->visitedSWC = SWC;
}

PathCard & PathCard::operator=(const PathCard & otherCard)
{
	this->type = otherCard.type;
	this->pathToCenter = otherCard.pathToCenter;
	this->pathToEast = otherCard.pathToEast;
	this->pathToNorth = otherCard.pathToNorth;
	this->pathToWest = otherCard.pathToWest;
	this->pathToSouth = otherCard.pathToSouth;
	this->isGold = otherCard.isGold;
	this->isRotate = otherCard.isRotate;
	this->canConnectTo = otherCard.canConnectTo;
	this->haveCrystal = otherCard.haveCrystal;
	this->isAnEntry = otherCard.isAnEntry;

	this->visitedNSC = otherCard.visitedNSC;
	this->visitedWEC = otherCard.visitedWEC;

	this->visitedNWC = otherCard.visitedNWC;
	this->visitedSEC = otherCard.visitedSEC;

	this->visitedNEC = otherCard.visitedNEC;
	this->visitedSWC = otherCard.visitedSWC;

	return *this;
}

PathCard & PathCard::operator=(PathCard && otherCard)
{
	*(this) = otherCard;
	new(&otherCard) PathCard;
	return *this;
}

void PathCard::setPossiblePaths(const Enums::PathCardType & type)
{
	//if (this->type != type)
	//	this->type = type;

	setPathToNorth(false);
	setPathToSouth(false);
	setPathToWest(false);
	setPathToEast(false);
	setPathToCenter(false);

	switch (type)
	{
	case Enums::PathCardType::Undefined:
		setPathToNorth(false);
		setPathToSouth(false);
		setPathToWest(false);
		setPathToEast(false);
		setPathToCenter(false);
		break;
	case Enums::PathCardType::WEC:
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::SWEC:
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSWEC:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::SWC:
		setPathToSouth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NWC:
		setPathToNorth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSEC:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSC:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToCenter(true);
		break;

		/* Expansion */
	case Enums::PathCardType::SEC_W:
		setPathToSouth(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WEC_N_S:
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WEC_S:
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NEC_SWC:
		// SPECIAL CASE
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NWC_SEC:
		// SPECIAL CASE
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSC_W_E:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::SWC_E:
		setPathToSouth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;


		/* Team Doors */
	case Enums::PathCardType::NSC_B:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NWC_B:
		setPathToNorth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WEC_B:
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSC_G:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::SWC_G:
		setPathToSouth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WEC_S_G:
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NEC_T:
		setPathToNorth(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WC_T:
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::SEC_T:
		setPathToSouth(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NC_T:
		setPathToNorth(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WEC_NSC: 
		// SPECIAL CASE
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		isWEC_NSC = true;
		break;
	case Enums::PathCardType::NC_C:
		setPathToNorth(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WC_C:
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSWC_C:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSWC_E_C:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::SWEC_C:
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NWEC_S_C:
		setPathToNorth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSWEC_C:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSC_W_C:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToCenter(true);
		break;
	default:
		break;
	}
	isRotate = false;
}

void PathCard::setPossibleConnections(const Enums::PathCardType & type)
{
	bool north = false, south = false, west = false, east = false;

	switch (type)
	{
	case Enums::PathCardType::SEC_W:
		west = true;
		break;
	case Enums::PathCardType::WEC_N_S:
		north = true;
		south = true;
		break;
	case Enums::PathCardType::WEC_S:
		south = true;
		break;
	case Enums::PathCardType::NEC_SWC:
		// SPECIAL CASE
		break;
	case Enums::PathCardType::NWC_SEC:
		// SPECIAL CASE
		break;
	case Enums::PathCardType::NSC_W_E:
		west = true;
		east = true;
		break;
	case Enums::PathCardType::SWC_E:
		east = true;
		break;
	case Enums::PathCardType::WEC_S_G:
		south = true;
		break;
	case Enums::PathCardType::NSWC_E_C:
		east = true;
		break;
	case Enums::PathCardType::NWEC_S_C:
		south = true;
		break;
	case Enums::PathCardType::NSC_W_C:
		west = true;
		break;
	case Enums::PathCardType::WC:
		west = true;
		break;
	case Enums::PathCardType::NWE:
		north = true;
		west = true;
		east = true;
		break;
	case Enums::PathCardType::NSWE:
		north = true;
		south = true;
		west = true;
		east = true;
		break;
	case Enums::PathCardType::SW:
		south = true;
		west = true;
		break;
	case Enums::PathCardType::NW:
		north = true;
		west = true;
		break;
	case Enums::PathCardType::NC:
		north = true;
		break;
	case Enums::PathCardType::WE:
		west = true;
		east = true;
		break;
	case Enums::PathCardType::NSE:
		north = true;
		south = true;
		east = true;
		break;
	case Enums::PathCardType::NS:
		north = true;
		south = true;
		break;
	default:
		break;
	}
	this->canConnectTo = std::make_tuple(north, south, west, east);
}

void PathCard::setIfHaveCrystal(const Enums::PathCardType & type)
{
	if (type == Enums::PathCardType::NC_C ||
		type == Enums::PathCardType::WC_C ||
		type == Enums::PathCardType::NSWC_C ||
		type == Enums::PathCardType::NSWC_E_C ||
		type == Enums::PathCardType::SWEC_C ||
		type == Enums::PathCardType::NWEC_S_C ||
		type == Enums::PathCardType::NSWEC_C ||
		type == Enums::PathCardType::NSC_W_C)
	{
		this->haveCrystal = true;
	}
}

void PathCard::setIfIsEntry(const Enums::PathCardType & type)
{
	if (type == Enums::PathCardType::NC_T ||
		type == Enums::PathCardType::NEC_T ||
		type == Enums::PathCardType::SEC_T ||
		type == Enums::PathCardType::WC_T)
	{
		this->isAnEntry = true;
	}
}



void PathCard::rotate()
{
	isRotate = !isRotate;
	if (pathToNorth && !pathToSouth)
	{
		pathToSouth = true;
		pathToNorth = false;
	}
	else if (pathToSouth && !pathToNorth)
	{
		pathToNorth = true;
		pathToSouth = false;
	}

	if (pathToWest && !pathToEast)
	{
		pathToEast = true;
		pathToWest = false;
	}
	else if (pathToEast && !pathToWest)
	{
		pathToWest = true;
		pathToEast = false;
	}
}

void PathCard::Print()
{
}
