#pragma once
class Enums
{
public:
	enum class ActionCardType
	{
		Undefined,

		RepairPickaxe,
		RepairCart,
		RepairLamp,

		RepairPickaxeAndLamp,
		RepairPickaxeAndCart,
		RepairLampAndCart,

		BrokenPickaxe,
		BrokenCart,
		BrokenLamp,

		Map,
		RockFall,

		/* Expansion */
		SwapYourHats,
		HandsOff,
		SwapYourHand,
		FreeAtLeast,
		Theft,
		Trapped,
		Inspection

	};

	enum class PathCardType
	{
		Undefined,
		WEC, // x4
		SWEC, // x5
		NSWEC, // x5
		SWC, // x4
		NWC, // x5
		WC, // x1
		NWE, // x1
		NSWE, // x1
		SW, // x1
		NW, // x1
		NC, // x1
		NSEC, // x5
		NSC, // x3
		WE, // x1
		NSE, // x1
		NS, // x1

		/* New Path Types */
		SEC_W,
		WEC_N_S,
		WEC_S,
		NEC_SWC,
		NWC_SEC,
		NSC_W_E,
		SWC_E,

		/* Team Doors */
		NSC_B,
		NWC_B,
		WEC_B,
		NSC_G,
		SWC_G,
		WEC_S_G,

		/* Tunnels */
		NEC_T,
		WC_T,
		SEC_T,
		NC_T,

		WEC_NSC, // BRIDGE

		/* Path with Crystals */
		NC_C,
		WC_C,
		NSWC_C,
		NSWC_E_C,
		SWEC_C,
		NWEC_S_C,
		NSWEC_C,
		NSC_W_C,
	};

	enum class PlayerType
	{
		Undefinded,
		GreenTeam,
		BlueTeam,
		Saboteur,
		Boss,
		Profiteer,
		Geologist
	};
};