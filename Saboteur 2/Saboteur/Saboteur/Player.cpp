#include "Player.h"

Player::Player(const std::string & name, const int& age, const Enums::PlayerType& type, bool const& haveTheft, bool const& isTrapped) :
	name(name),
	age(age),
	type(type),
	gold(0),
	intactCart(true),
	intactLamp(true),
	intactPickAxe(true),
	haveTheft(haveTheft),
	isTrapped(isTrapped),
	isComputer(false)
{
}

Player::Player (const Player & other)
{
	*this = other;
}

std::string Player::getName()
{
	return this->name;
}

int Player::getAge()
{
	return this->age;
}

uint8_t Player::getGold()
{
	return this->gold;
}

Enums::PlayerType Player::getType()
{
	return this->type;
}

CardDeck & Player::getCards()
{
	return cards;
}

bool Player::getPickaxeState() const
{
	return this->intactPickAxe;
}

bool Player::getCartState() const
{
	return this->intactCart;
}

bool Player::getLampState() const
{
	return this->intactLamp;
}

bool Player::getTheftState() const
{
	return this->haveTheft;
}

bool Player::getTrappedState() const
{
	return this->isTrapped;
}

bool Player::getIsComputer() const
{
	return this->isComputer;
}

void Player::setName(const std::string & name)
{
	this->name = name;
}

void Player::setAge(const int & age)
{
	this->age = age;
}

void Player::setType(const Enums::PlayerType & type)
{
	this->type = type;
}

void Player::setPickaxe(const bool & state)
{
	this->intactPickAxe = state;
}

void Player::setCart(const bool & state)
{
	this->intactCart = state;
}

void Player::setLamp(const bool & state)
{
	this->intactLamp = state;
}

void Player::setGold(const uint8_t & gold)
{
	this->gold = gold;
}

void Player::setTheft(const bool & state)
{
	this->haveTheft = state;
}

void Player::setTrapped(const bool & state)
{
	this->isTrapped = state;
}

void Player::setIsComputer(const bool & state)
{
	this->isComputer = state;
}

Player & Player::operator=(const Player & other)
{
	this->name = other.name;
	this->age = other.age;
	this->gold = other.gold;
	this->type = other.type;
	this->intactCart = other.intactCart;
	this->intactLamp = other.intactLamp;
	this->intactPickAxe = other.intactPickAxe;
	return *this;
}


