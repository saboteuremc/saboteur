#include "Game.h"
//for the dll
#include "../../DLL/DLL/SaboteurV1.h"
#pragma comment(lib, "DLL.lib")
#include <fstream> 
std::ofstream of("syslog2.log", std::ios::app);
std::string myString = "";
Saboteur mySaboteurDLL(of);
auto dllCall = [](std::string& myString, Saboteur mySaboteurDLL, Saboteur::Actions actionType, Saboteur::Type type)
{
	myString = mySaboteurDLL.showMessage(actionType, type);
	mySaboteurDLL.log(myString);
};
void Game::initPathCardDeck()
{
	int index = 0;
	initPathCards(index);
	initActionCards(index);
}

void Game::initPathCards(int& index)
{
	this->pathCardDeck.setSize(117);

	auto addCard = [](const int& count, const Enums::PathCardType& type, CardDeck& pathCardDeck, int& index)
	{
		for (int i = 0; i < count; i++)
		{
			pathCardDeck[index++] = new PathCard(type);
		};
	};

	addCard(5, Enums::PathCardType::SWEC, pathCardDeck, index);
	addCard(5, Enums::PathCardType::NSWEC, pathCardDeck, index);
	addCard(5, Enums::PathCardType::NSEC, pathCardDeck, index);
	addCard(5, Enums::PathCardType::NWC, pathCardDeck, index);


	addCard(4, Enums::PathCardType::WEC, pathCardDeck, index);
	addCard(4, Enums::PathCardType::SWC, pathCardDeck, index);

	addCard(3, Enums::PathCardType::NSC, pathCardDeck, index);

	addCard(1, Enums::PathCardType::WC, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NW, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NWE, pathCardDeck, index);
	addCard(1, Enums::PathCardType::SW, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NC, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSE, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NS, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSWE, pathCardDeck, index);
	addCard(1, Enums::PathCardType::WE, pathCardDeck, index);

	/* Expansion */
	addCard(1, Enums::PathCardType::SEC_W, pathCardDeck, index);
	addCard(1, Enums::PathCardType::WEC_N_S, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NWC_SEC, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSC_W_E, pathCardDeck, index);
	addCard(1, Enums::PathCardType::WEC_S, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NEC_SWC, pathCardDeck, index);
	addCard(1, Enums::PathCardType::SWC_E, pathCardDeck, index);
	addCard(1, Enums::PathCardType::SWC_E, pathCardDeck, index);

	addCard(1, Enums::PathCardType::NSC_B, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NWC_B, pathCardDeck, index);
	addCard(1, Enums::PathCardType::WEC_B, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSC_G, pathCardDeck, index);
	addCard(1, Enums::PathCardType::SWC_G, pathCardDeck, index);
	addCard(1, Enums::PathCardType::WEC_S_G, pathCardDeck, index);

	addCard(1, Enums::PathCardType::NEC_T, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NC_T, pathCardDeck, index);
	addCard(1, Enums::PathCardType::SEC_T, pathCardDeck, index);
	addCard(1, Enums::PathCardType::WC_T, pathCardDeck, index);

	addCard(1, Enums::PathCardType::WEC_NSC, pathCardDeck, index);

	addCard(1, Enums::PathCardType::NC_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSC_W_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSWC_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSWC_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSWC_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSWC_E_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSWEC_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NWEC_S_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::SWEC_C, pathCardDeck, index);
	addCard(1, Enums::PathCardType::WC_C, pathCardDeck, index);

}

void Game::initActionCards(int& index)
{

	auto addCard = [](const int& count, const Enums::ActionCardType& type, CardDeck& pathCardDeck, int& index)
	{
		for (int i = 0; i < count; i++)
		{
			pathCardDeck[index++] = new ActionCard(type);
		};
	};

	addCard(2, Enums::ActionCardType::RepairPickaxe, pathCardDeck, index);
	addCard(2, Enums::ActionCardType::RepairCart, pathCardDeck, index);
	addCard(2, Enums::ActionCardType::RepairLamp, pathCardDeck, index);

	addCard(3, Enums::ActionCardType::BrokenPickaxe, pathCardDeck, index);
	addCard(3, Enums::ActionCardType::BrokenCart, pathCardDeck, index);
	addCard(3, Enums::ActionCardType::BrokenLamp, pathCardDeck, index);

	addCard(6, Enums::ActionCardType::Map, pathCardDeck, index);

	addCard(3, Enums::ActionCardType::RockFall, pathCardDeck, index);

	addCard(1, Enums::ActionCardType::RepairPickaxeAndCart, pathCardDeck, index);

	addCard(1, Enums::ActionCardType::RepairLampAndCart, pathCardDeck, index);

	addCard(1, Enums::ActionCardType::RepairPickaxeAndLamp, pathCardDeck, index);

	/* Extension */
	addCard(3, Enums::ActionCardType::Trapped, pathCardDeck, index);
	addCard(4, Enums::ActionCardType::Theft, pathCardDeck, index);
	addCard(2, Enums::ActionCardType::SwapYourHats, pathCardDeck, index);
	addCard(3, Enums::ActionCardType::HandsOff, pathCardDeck, index);

	addCard(2, Enums::ActionCardType::SwapYourHand, pathCardDeck, index);

	addCard(2, Enums::ActionCardType::Inspection, pathCardDeck, index);
	addCard(4, Enums::ActionCardType::FreeAtLeast, pathCardDeck, index);

	addCard(1, Enums::ActionCardType::RockFall, pathCardDeck, index);
}

void Game::initGoldCardDeck()
{

	auto addCard = [](const int& count, const int& amount, std::vector<uint8_t>& goldCardDeck)
	{
		for (int i = 0; i < count; i++)
		{
			goldCardDeck.push_back(amount);
		};
	};

	int goldCardNumbers = 17;
	addCard(goldCardNumbers, 1, goldCardDeck);

	goldCardNumbers = 7;
	addCard(goldCardNumbers, 2, goldCardDeck);

	goldCardNumbers = 4;
	addCard(goldCardNumbers, 3, goldCardDeck);

}

void Game::initDwarfCardDeck()
{
	if (dwarfCardDeck.size() != 0)
	{
		dwarfCardDeck.clear();
	}

	auto addCard = [](const int& count, const Enums::PlayerType& type, std::vector<Enums::PlayerType>& dwarfCardDeck)
	{
		for (int i = 0; i < count; i++)
		{
			dwarfCardDeck.push_back(type);
		};
	};

	addCard(3, Enums::PlayerType::Saboteur, dwarfCardDeck);
	addCard(4, Enums::PlayerType::GreenTeam, dwarfCardDeck);
	addCard(4, Enums::PlayerType::BlueTeam, dwarfCardDeck);

	addCard(1, Enums::PlayerType::Boss, dwarfCardDeck);
	addCard(1, Enums::PlayerType::Profiteer, dwarfCardDeck);
	addCard(2, Enums::PlayerType::Geologist, dwarfCardDeck);

	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::shuffle(dwarfCardDeck.begin(), dwarfCardDeck.end(), generator);
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::cardsShuffled, Saboteur::Type::Info);
}

void Game::distributePathCard(const int & numberOfPlayers, Player * player, int& index)
{
	int numberOfCards = 6;

	player->getCards().setSize(numberOfCards);
	for (int i = 0; i < numberOfCards; i++)
	{
		player->getCards()[i] = std::move(pathCardDeck[index++]);
		pathCardDeck.getDeck().erase(pathCardDeck.getDeck().begin() + i);
		player->getCards()[i].value()->priority =  this->getPriority(player, player->getCards()[i].value());
	}
	index = 0;
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::playersReady, Saboteur::Type::Info);
}

void Game::distributeDwarfCards()
{
	int count = 0;
	for (auto player : players)
	{
		player->setType(dwarfCardDeck[count]);
		if (dwarfCardDeck[count] == Enums::PlayerType::GreenTeam ||
			dwarfCardDeck[count] == Enums::PlayerType::BlueTeam)
		{
			this->nrOfDwarf++;
		}
		count++;
	}
}

Game::Game()
{
	this->round = 0;
	this->nrOfCrystals = 0;
	this->nrOfDwarf = 0;
	this->pathCardDeck.setSize(117);
}



bool Game::allPlayersHaveEmptyHands()
{
	return std::all_of(players.begin(), players.end(), [](Player* player) {return player->getCards().getDeck().empty(); });
}


Board& Game::getBoard()
{
	return this->board;
}

std::vector<Player*>& Game::getPlayers()
{
	return this->players;
}

CardDeck& Game::getPathCardDeck()
{
	return this->pathCardDeck;
}

CardDeck& Game::getDiscardDeck()
{
	return this->discardDeck;
}

std::vector<Enums::PlayerType>& Game::getDwarfCardDeck()
{
	return this->dwarfCardDeck;
}

std::vector<uint8_t>& Game::getGoldCardDeck()
{
	return this->goldCardDeck;
}

int Game::getNumberOfPlayers()
{
	return this->numberOfPlayers;
}

int Game::getRound()
{
	return this->round;
}

void Game::setBoard(Board board)
{
	this->board = board;
}

void Game::setPlayers(std::vector<Player*> players)
{
	this->players = players;
}

void Game::setPathCardDeck(CardDeck pathCardDeck)
{
	this->pathCardDeck = pathCardDeck;
}

void Game::setDiscardDeck(CardDeck discardDeck)
{
	this->discardDeck = discardDeck;
}

void Game::setDwarfCardDeck(std::vector<Enums::PlayerType> dwarfCardDeck)
{
	this->dwarfCardDeck = dwarfCardDeck;
}

void Game::setGoldcardDeck(std::vector<uint8_t> goldCardDeck)
{
	this->goldCardDeck = goldCardDeck;
}

void Game::setNumberOfPlayers(const int & numberOfPlayers)
{
	this->numberOfPlayers = numberOfPlayers;
}

void Game::extractTenCards()
{
	for (int i = 0; i < 10; ++i)
	{
		this->pathCardDeck.getDeck().erase(this->pathCardDeck.getDeck().end() - 1);
	}
}

bool Game::canPutCardOnBoard(Board board, Board::Position position, PathCard* pathCard)
{
	auto northPosition = Board::Position(position.first - 1, position.second);
	auto southPosition = Board::Position(position.first + 1, position.second);
	auto westPosition = Board::Position(position.first, position.second - 1);
	auto eastPosition = Board::Position(position.first, position.second + 1);

	bool north = false, south = false, east = false, west = false;
	bool isPathFirst = false, isPathSecond = false;

	auto[canConnectToNorth, canConnectToSouth, canConnectToWest, canConnectToEast] = pathCard->getCanConnectTo();

	bool itHaveAtLeastOneOpening = false;

	if (board[position] == std::nullopt)
	{

		//verify if all is null
		try
		{
			if (board.inBound(northPosition) && board[northPosition] == std::nullopt &&
				board.inBound(southPosition) && board[southPosition] == std::nullopt &&
				board.inBound(eastPosition) && board[eastPosition] == std::nullopt &&
				board.inBound(westPosition) && board[westPosition] == std::nullopt)
			{
				return false;
			}
		}
		catch (const char* message)
		{
			return false;
		}

		//north
		if (!board.inBound(northPosition))
		{
			north = true;
		}
		else
		{
			auto northCardConnect = std::make_tuple(false, false, false, false);
			if (board.inBound(northPosition) && board[northPosition].has_value())
			{
				northCardConnect = board[northPosition].value().getCanConnectTo();
			}

			if (board.inBound(northPosition) && board[northPosition] == std::nullopt)
			{
				north = true;
				itHaveAtLeastOneOpening = true;
			}
			else
			{
				isPathFirst = false;
				isPathSecond = false;

				if (board[northPosition].value().getPathToSouth() || std::get<1>(northCardConnect))
					isPathFirst = true;

				if (pathCard->getPathToNorth() || canConnectToNorth)
					isPathSecond = true;

				if (isPathFirst && isPathSecond)
				{
					north = true;
				}
				else if (!isPathFirst && !isPathSecond)
				{
					north = true;
				}
			}
		}

		//verify west
		if (!board.inBound(westPosition))
		{
			west = true;
		}
		else
		{
			auto westCardConnect = std::make_tuple(false, false, false, false);
			if (board.inBound(westPosition) && board[westPosition].has_value())
			{
				westCardConnect = board[westPosition].value().getCanConnectTo();
			}

			if (board.inBound(westPosition) && board[westPosition] == std::nullopt)
			{
				west = true;
				itHaveAtLeastOneOpening = true;
			}
			else
			{
				isPathFirst = false;
				isPathSecond = false;

				if ((board[westPosition].value().getPathToEast() || std::get<3>(westCardConnect)))
					isPathFirst = true;

				if (pathCard->getPathToWest() || canConnectToWest)
					isPathSecond = true;

				if (isPathFirst && isPathSecond)
				{
					west = true;
				}
				else if (!isPathFirst && !isPathSecond)
				{
					west = true;
				}
			}
		}

		//verify south
		if (!board.inBound(southPosition))
		{
			south = true;
		}
		else
		{
			auto southCardConnect = std::make_tuple(false, false, false, false);
			if (board.inBound(southPosition) && board[southPosition].has_value())
			{
				southCardConnect = board[southPosition].value().getCanConnectTo();
			}
			if (board.inBound(southPosition) && board[southPosition] == std::nullopt)
			{
				south = true;
				itHaveAtLeastOneOpening = true;
			}
			else
			{
				isPathFirst = false;
				isPathSecond = false;

				if ((board[southPosition].value().getPathToNorth() || std::get<0>(southCardConnect)))
					isPathFirst = true;

				if (pathCard->getPathToSouth() || canConnectToSouth)
					isPathSecond = true;

				if (isPathFirst && isPathSecond)
				{
					south = true;
				}
				else if (!isPathFirst && !isPathSecond)
				{
					south = true;
				}
			}
		}


		//verify east
		if (!board.inBound(eastPosition))
		{
			east = true;
		}
		else
		{
			auto eastCardConnect = std::make_tuple(false, false, false, false);
			if (board.inBound(eastPosition) && board[eastPosition].has_value())
			{
				eastCardConnect = board[eastPosition].value().getCanConnectTo();
			}
			if (board.inBound(eastPosition) && board[eastPosition] == std::nullopt)
			{
				east = true;
				itHaveAtLeastOneOpening = true;
			}
			else
			{
				isPathFirst = false;
				isPathSecond = false;

				if ((board[eastPosition].value().getPathToWest() || std::get<2>(eastCardConnect)))
					isPathFirst = true;

				if (pathCard->getPathToEast() || canConnectToEast)
					isPathSecond = true;

				if (isPathFirst && isPathSecond)
				{
					east = true;
				}
				else if (!isPathFirst && !isPathSecond)
				{
					east = true;
				}
			}
		}

	}

	if (east && north && west && south)
	{
		board[position] = *pathCard;
		north = allowEntryPlacement(board, position);
		board[position] = std::nullopt;
	}

	if (east && north && west && south)
	{
		pathCardsPosition.push_back(position);
	}

	return (east && north && west && south);
}

bool Game::allowEntryPlacement(Board& board, Board::Position position)
{
	std::queue<Board::Position> path;
	std::vector<Board::Position> visited;

	std::vector<std::pair<Board::Position, PathCard>> resetCoord;

	int count = 0;

	path.push(position);
	visited.push_back(position);
	while (!path.empty())
	{
		auto front = path.front();
		if (board[front].value().getIfIsEntry())
		{
			count++;
		}

		//west
		auto westPosition = Board::Position(front.first, front.second - 1);
		if (inBound(westPosition))
		{
			if ((board[westPosition].has_value() &&
				board[front].value().getPathToWest() &&
				board[westPosition].value().getPathToEast() &&
				board[front].value().getPathToCenter() &&
				std::find(visited.begin(), visited.end(), westPosition) == visited.end())
				||
				(board[westPosition].has_value() &&
					board[westPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
					std::find(visited.begin(), visited.end(), westPosition) == visited.end())
				||
				(board[westPosition].has_value() &&
					board[westPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
					std::find(visited.begin(), visited.end(), westPosition) == visited.end())
				||
				(board[westPosition].has_value() &&
					board[westPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
					std::find(visited.begin(), visited.end(), westPosition) == visited.end()))
			{
				if (board[westPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
					board[westPosition].value().getVisitedWEC() == false)
				{
					board[westPosition].value().setPossiblePaths(Enums::PathCardType::WEC);
					board[westPosition].value().setVisitedWEC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(westPosition, PathCard(Enums::PathCardType::WEC_NSC, false)));

					path.push(westPosition);
				}
				else if (board[westPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
					board[westPosition].value().getVisitedSEC() == false)
				{
					board[westPosition].value().setPossiblePaths(Enums::PathCardType::NWC);
					board[westPosition].value().rotate();
					board[westPosition].value().setVisitedSEC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(westPosition, PathCard(Enums::PathCardType::NWC_SEC, false)));

					path.push(westPosition);
				}
				else if (board[westPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
					board[westPosition].value().getVisitedNEC() == false)
				{
					board[westPosition].value().setPossiblePaths(Enums::PathCardType::SWC);
					board[westPosition].value().rotate();
					board[westPosition].value().setVisitedNEC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(westPosition, PathCard(Enums::PathCardType::NEC_SWC, false)));

					path.push(westPosition);
				}
				else if (board[westPosition].value().getType() != Enums::PathCardType::WEC_NSC &&
					board[westPosition].value().getType() != Enums::PathCardType::NWC_SEC &&
					board[westPosition].value().getType() != Enums::PathCardType::NEC_SWC)
				{
					path.push(westPosition);
					visited.push_back(westPosition);
				}
			}
		}

		//south
		auto southPosition = Board::Position(front.first + 1, front.second);
		if (inBound(southPosition))
		{
			if ((board[southPosition].has_value() &&
				board[front].value().getPathToSouth() &&
				board[front].value().getPathToCenter() &&
				board[southPosition].value().getPathToNorth() &&
				std::find(visited.begin(), visited.end(), southPosition) == visited.end())
				||
				(board[southPosition].has_value() &&
					board[southPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
					std::find(visited.begin(), visited.end(), southPosition) == visited.end())
				||
				(board[southPosition].has_value() &&
					board[southPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
					std::find(visited.begin(), visited.end(), southPosition) == visited.end())
				||
				(board[southPosition].has_value() &&
					board[southPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
					std::find(visited.begin(), visited.end(), southPosition) == visited.end()))
			{
				if (board[southPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
					board[southPosition].value().getVisitedNSC() == false)
				{
					board[southPosition].value().setPossiblePaths(Enums::PathCardType::NSC);
					board[southPosition].value().setVisitedNSC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(southPosition, PathCard(Enums::PathCardType::WEC_NSC, false)));

					path.push(southPosition);
				}
				else if (board[southPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
					board[southPosition].value().getVisitedNWC() == false)
				{
					board[southPosition].value().setPossiblePaths(Enums::PathCardType::NWC);
					board[southPosition].value().setVisitedNWC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(southPosition, PathCard(Enums::PathCardType::NWC_SEC, false)));

					path.push(southPosition);
				}
				else if (board[southPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
					board[southPosition].value().getVisitedNEC() == false)
				{
					board[southPosition].value().setPossiblePaths(Enums::PathCardType::SWC);
					board[southPosition].value().rotate();
					board[southPosition].value().setVisitedNEC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(southPosition, PathCard(Enums::PathCardType::NEC_SWC, false)));

					path.push(southPosition);
				}
				else if (board[southPosition].value().getType() != Enums::PathCardType::WEC_NSC &&
					board[southPosition].value().getType() != Enums::PathCardType::NWC_SEC &&
					board[southPosition].value().getType() != Enums::PathCardType::NEC_SWC)
				{
					path.push(southPosition);
					visited.push_back(southPosition);
				}
			}
		}

		//east
		auto eastPosition = Board::Position(front.first, front.second + 1);
		if (inBound(eastPosition))
		{
			if ((board[eastPosition].has_value() &&
				board[front].value().getPathToEast() &&
				board[front].value().getPathToCenter() &&
				board[eastPosition].value().getPathToWest() &&
				std::find(visited.begin(), visited.end(), eastPosition) == visited.end())
				||
				(board[eastPosition].has_value() &&
					board[eastPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
					std::find(visited.begin(), visited.end(), eastPosition) == visited.end())
				||
				(board[eastPosition].has_value() &&
					board[eastPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
					std::find(visited.begin(), visited.end(), eastPosition) == visited.end())
				||
				(board[eastPosition].has_value() &&
					board[eastPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
					std::find(visited.begin(), visited.end(), eastPosition) == visited.end()))
			{
				if (board[eastPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
					board[eastPosition].value().getVisitedWEC() == false)
				{
					board[eastPosition].value().setPossiblePaths(Enums::PathCardType::WEC);
					board[eastPosition].value().setVisitedWEC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(eastPosition, PathCard(Enums::PathCardType::WEC_NSC, false)));

					path.push(eastPosition);
				}
				else if (board[eastPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
					board[eastPosition].value().getVisitedNWC() == false)
				{
					board[eastPosition].value().setPossiblePaths(Enums::PathCardType::NWC);
					board[eastPosition].value().setVisitedNWC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(eastPosition, PathCard(Enums::PathCardType::NWC_SEC, false)));

					path.push(eastPosition);
				}
				else if (board[eastPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
					board[eastPosition].value().getVisitedSWC() == false)
				{
					board[eastPosition].value().setPossiblePaths(Enums::PathCardType::SWC);
					board[eastPosition].value().setVisitedSWC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(eastPosition, PathCard(Enums::PathCardType::NEC_SWC, false)));

					path.push(eastPosition);
				}
				else if (board[eastPosition].value().getType() != Enums::PathCardType::WEC_NSC &&
					board[eastPosition].value().getType() != Enums::PathCardType::NWC_SEC &&
					board[eastPosition].value().getType() != Enums::PathCardType::NEC_SWC)
				{
					path.push(eastPosition);
					visited.push_back(eastPosition);
				}
			}
		}

		//north
		auto northPosition = Board::Position(front.first - 1, front.second);
		if (inBound(northPosition))
		{
			if ((board[northPosition] &&
				board[front].value().getPathToNorth() &&
				board[front].value().getPathToCenter() &&
				board[northPosition].value().getPathToSouth() &&
				std::find(visited.begin(), visited.end(), northPosition) == visited.end())
				||
				(board[northPosition].has_value() &&
					board[northPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
					std::find(visited.begin(), visited.end(), northPosition) == visited.end())
				||
				(board[northPosition].has_value() &&
					board[northPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
					std::find(visited.begin(), visited.end(), northPosition) == visited.end())
				||
				(board[northPosition].has_value() &&
					board[northPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
					std::find(visited.begin(), visited.end(), northPosition) == visited.end()))
			{
				if (board[northPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
					board[northPosition].value().getVisitedNSC() == false)
				{
					board[northPosition].value().setPossiblePaths(Enums::PathCardType::NSC);
					board[northPosition].value().setVisitedNSC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(northPosition, PathCard(Enums::PathCardType::WEC_NSC, false)));

					path.push(northPosition);
				}
				else if (board[northPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
					board[northPosition].value().getVisitedSEC() == false)
				{
					board[northPosition].value().setPossiblePaths(Enums::PathCardType::NWC);
					board[northPosition].value().rotate();
					board[northPosition].value().setVisitedSEC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(northPosition, PathCard(Enums::PathCardType::NWC_SEC, false)));

					path.push(northPosition);
				}
				else if (board[northPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
					board[northPosition].value().getVisitedSWC() == false)
				{
					board[northPosition].value().setPossiblePaths(Enums::PathCardType::SWC);
					board[northPosition].value().setVisitedSWC(true);

					resetCoord.push_back(std::pair<Board::Position, PathCard>(northPosition, PathCard(Enums::PathCardType::NEC_SWC, false)));

					path.push(northPosition);
				}
				else if (board[northPosition].value().getType() != Enums::PathCardType::WEC_NSC &&
					board[northPosition].value().getType() != Enums::PathCardType::NWC_SEC &&
					board[northPosition].value().getType() != Enums::PathCardType::NEC_SWC)
				{
					path.push(northPosition);
					visited.push_back(northPosition);
				}
			}
		}
		path.pop();
	}

	for (std::pair<Board::Position, PathCard> card : resetCoord)
	{
		board[card.first] = card.second;
	}
	resetCoord.clear();

	if (count > 1)
	{
		return false;
	}
	return true;
}

Player* Game::actionCardAffectedPlayer(const Enums::ActionCardType& type, Player * currentPlayer)
{

	if (type == Enums::ActionCardType::FreeAtLeast && currentPlayer->getTrappedState())
		return currentPlayer;
	else if (type == Enums::ActionCardType::RepairCart && !currentPlayer->getCartState())
		return currentPlayer;
	else if (type == Enums::ActionCardType::RepairLamp && !currentPlayer->getLampState())
		return currentPlayer;
	else if (type == Enums::ActionCardType::RepairPickaxe && !currentPlayer->getPickaxeState())
		return currentPlayer;
	else if (type == Enums::ActionCardType::RepairLampAndCart && (!currentPlayer->getLampState() || !currentPlayer->getCartState()))
		return currentPlayer;
	else if (type == Enums::ActionCardType::RepairPickaxeAndCart && (!currentPlayer->getPickaxeState() || !currentPlayer->getCartState()))
		return currentPlayer;
	else if (type == Enums::ActionCardType::RepairPickaxeAndLamp && (!currentPlayer->getLampState() || !currentPlayer->getPickaxeState()))
		return currentPlayer;
	else if (type == Enums::ActionCardType::Theft && (!currentPlayer->getTheftState()))
		return currentPlayer;
	else if (type == Enums::ActionCardType::SwapYourHats ||
		type == Enums::ActionCardType::SwapYourHand ||
		type == Enums::ActionCardType::Map ||
		type == Enums::ActionCardType::RockFall)
		return currentPlayer;

	for (auto player : players)
	{
		if (player != currentPlayer)
		{
			switch (type)
			{
			case Enums::ActionCardType::BrokenCart:
				if (player->getCartState() && (player != currentPlayer))
					return player;
			case Enums::ActionCardType::BrokenLamp:
				if (player->getLampState() && (player != currentPlayer))
					return player;
			case Enums::ActionCardType::BrokenPickaxe:
				if (player->getPickaxeState() && (player != currentPlayer))
					return player;
			case Enums::ActionCardType::HandsOff:
				if (player->getTheftState() && (player != currentPlayer))
					return player;
			case Enums::ActionCardType::Trapped:
				if (!player->getTrappedState() && (player != currentPlayer))
					return player;

			default: return nullptr;
			}
		}
	}
	return nullptr;
}

int Game::getPriority(Player* player, Card * card)
{
	PathCard* pathCard = dynamic_cast<PathCard*>(card);
	ActionCard* actionCard = dynamic_cast<ActionCard*>(card);
	auto playerType = player->getType();

	if (playerType == Enums::PlayerType::Saboteur)
	{
		if (actionCard != nullptr)
		{
			Enums::ActionCardType type = actionCard->getType();

			if (type == Enums::ActionCardType::FreeAtLeast && player->getTrappedState())
			{
				return 11;
			}
			else if (type == Enums::ActionCardType::Theft)
			{
				return 10;
			}
			else if (type == Enums::ActionCardType::Trapped)
			{
				return 9;
			}
			else if (type == Enums::ActionCardType::HandsOff)
			{
				return 8;
			}
			else if (type == Enums::ActionCardType::RockFall)
			{
				return 7;
			}
			else if ((type == Enums::ActionCardType::BrokenCart ||
					type == Enums::ActionCardType::BrokenLamp ||
					type == Enums::ActionCardType::BrokenPickaxe) && 
					actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 6;
			}
			else if (type == Enums::ActionCardType::Inspection)
			{
				return 4;
			}
			else if ((type == Enums::ActionCardType::RepairCart ||
				type == Enums::ActionCardType::RepairLamp ||
				type == Enums::ActionCardType::RepairLampAndCart ||
				type == Enums::ActionCardType::RepairPickaxe ||
				type == Enums::ActionCardType::RepairPickaxeAndCart ||
				type == Enums::ActionCardType::RepairPickaxeAndLamp) && 
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 3;
			}
			else if (type == Enums::ActionCardType::Map)
			{
				return 2;
			}
			else if (type == Enums::ActionCardType::SwapYourHand)
			{
				return 1;
			}
			else if (type == Enums::ActionCardType::SwapYourHats)
			{
				return 0;
			}
		}
		else if (pathCard != nullptr)
		{
			const int pathPriority = 5;

			if(pathCard->getType() == Enums::PathCardType::NWE) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::NSWE) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::SW) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::NW) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::WE) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::NSE) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::NS) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::NC) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::WC) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::WC_T) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::NC_T) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::NC_C) return pathPriority;
			if(pathCard->getType() == Enums::PathCardType::WC_C) return pathPriority;
		
			return 0;
		}
	}
	else if (playerType == Enums::PlayerType::GreenTeam)
	{
		if (actionCard != nullptr)
		{
			Enums::ActionCardType type = actionCard->getType();

			if (type == Enums::ActionCardType::FreeAtLeast && player->getTrappedState())
			{
				return 12;
			}
			else if (type == Enums::ActionCardType::Theft)
			{
				return 11;
			}
			else if (type == Enums::ActionCardType::HandsOff)
			{
				return 10;
			}
			else if ((type == Enums::ActionCardType::RepairCart ||
				type == Enums::ActionCardType::RepairLamp ||
				type == Enums::ActionCardType::RepairLampAndCart ||
				type == Enums::ActionCardType::RepairPickaxe ||
				type == Enums::ActionCardType::RepairPickaxeAndCart ||
				type == Enums::ActionCardType::RepairPickaxeAndLamp )&& 
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 9;
			}
			else if (type == Enums::ActionCardType::Map)
			{
				return 5;
			}
			else if (type == Enums::ActionCardType::RockFall)
			{
				return 4;
			}
			else if (type == Enums::ActionCardType::Inspection)
			{
				return 3;
			}
			else if ((type == Enums::ActionCardType::BrokenCart ||
				type == Enums::ActionCardType::BrokenLamp ||
				type == Enums::ActionCardType::BrokenPickaxe) && 
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 2;
			}
			else if (type == Enums::ActionCardType::SwapYourHand)
			{
				return 1;
			}
			else if (type == Enums::ActionCardType::SwapYourHats)
			{
				return 0;
			}
		}
		else if (pathCard != nullptr)
		{
			const int highPriority = 8;
			const int mediumPriority = 7;
			const int lowPriority = 6;
			
			if(pathCard->getType() == Enums::PathCardType::NSWEC) return highPriority;
			if(pathCard->getType() == Enums::PathCardType::NSWEC_C) return highPriority;

			if(pathCard->getType() == Enums::PathCardType::NSC_G) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC_G) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_S_G) return highPriority;
			
			if(pathCard->getType() == Enums::PathCardType::NSC) return highPriority;
			if(pathCard->getType() == Enums::PathCardType::NSC_W_E) return highPriority;
			
			if(pathCard->getType() == Enums::PathCardType::WEC) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::SWEC) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::SWC) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NWC) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::WC) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NWE) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NSWE) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::SW) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NW) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NC) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NSEC) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::WE) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NSE) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NS) return mediumPriority;

			if(pathCard->getType() == Enums::PathCardType::SEC_W) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::WEC_N_S) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::WEC_S) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NEC_SWC) return lowPriority;
			if(pathCard->getType() == Enums::PathCardType::NWC_SEC) return lowPriority;
			if(pathCard->getType() == Enums::PathCardType::SWC_E) return mediumPriority;

			if(pathCard->getType() == Enums::PathCardType::NEC_T) return lowPriority;
			if(pathCard->getType() == Enums::PathCardType::WC_T) return lowPriority;
			if(pathCard->getType() == Enums::PathCardType::SEC_T) return lowPriority;
			if(pathCard->getType() == Enums::PathCardType::NC_T) return lowPriority;

			if(pathCard->getType() == Enums::PathCardType::WEC_NSC) return lowPriority;

			if(pathCard->getType() == Enums::PathCardType::NC_C) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::WC_C) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NSWC_C) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NSWC_E_C) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::SWEC_C) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NWEC_S_C) return mediumPriority;
			if(pathCard->getType() == Enums::PathCardType::NSC_W_C) return mediumPriority;

			return 0;
		}
	}
	else if (playerType == Enums::PlayerType::BlueTeam)
	{
		if (actionCard != nullptr)
		{
			Enums::ActionCardType type = actionCard->getType();

			if (type == Enums::ActionCardType::FreeAtLeast && player->getTrappedState())
			{
				return 12;
			}
			else if (type == Enums::ActionCardType::Theft)
			{
				return 11;
			}
			else if (type == Enums::ActionCardType::HandsOff)
			{
				return 10;
			}
			else if ((type == Enums::ActionCardType::RepairCart ||
				type == Enums::ActionCardType::RepairLamp ||
				type == Enums::ActionCardType::RepairLampAndCart ||
				type == Enums::ActionCardType::RepairPickaxe ||
				type == Enums::ActionCardType::RepairPickaxeAndCart ||
				type == Enums::ActionCardType::RepairPickaxeAndLamp) &&
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 9;
			}
			else if (type == Enums::ActionCardType::Map)
			{
				return 5;
			}
			else if (type == Enums::ActionCardType::RockFall)
			{
				return 4;
			}
			else if (type == Enums::ActionCardType::Inspection)
			{
				return 3;
			}
			else if ((type == Enums::ActionCardType::BrokenCart ||
				type == Enums::ActionCardType::BrokenLamp ||
				type == Enums::ActionCardType::BrokenPickaxe) &&
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 2;
			}
			else if (type == Enums::ActionCardType::SwapYourHand)
			{
				return 1;
			}
			else if (type == Enums::ActionCardType::SwapYourHats)
			{
				return 0;
			}
		}
		else if (pathCard != nullptr)
		{
			const int highPriority = 8;
			const int mediumPriority = 7;
			const int lowPriority = 6;

			if (pathCard->getType() == Enums::PathCardType::NSWEC) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWEC_C) return highPriority;

			if (pathCard->getType() == Enums::PathCardType::NSC_B) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC_B) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_B) return highPriority;

			if (pathCard->getType() == Enums::PathCardType::NSC) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSC_W_E) return highPriority;

			if (pathCard->getType() == Enums::PathCardType::WEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SW) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NW) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NS) return mediumPriority;

			if (pathCard->getType() == Enums::PathCardType::SEC_W) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_N_S) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_S) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NEC_SWC) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC_SEC) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC_E) return mediumPriority;

			if (pathCard->getType() == Enums::PathCardType::NEC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::WC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::SEC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::NC_T) return lowPriority;

			if (pathCard->getType() == Enums::PathCardType::WEC_NSC) return lowPriority;

			if (pathCard->getType() == Enums::PathCardType::NC_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WC_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWC_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWC_E_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWEC_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWEC_S_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSC_W_C) return mediumPriority;

			return 0;
		}
	}
	else if (playerType == Enums::PlayerType::Geologist)
	{
		if (actionCard != nullptr)
		{
			Enums::ActionCardType type = actionCard->getType();

			if (type == Enums::ActionCardType::FreeAtLeast && player->getTrappedState())
			{
				return 12;
			}
			else if (type == Enums::ActionCardType::Theft)
			{
				return 11;
			}
			else if (type == Enums::ActionCardType::HandsOff)
			{
				return 10;
			}
			else if ((type == Enums::ActionCardType::RepairCart ||
				type == Enums::ActionCardType::RepairLamp ||
				type == Enums::ActionCardType::RepairLampAndCart ||
				type == Enums::ActionCardType::RepairPickaxe ||
				type == Enums::ActionCardType::RepairPickaxeAndCart ||
				type == Enums::ActionCardType::RepairPickaxeAndLamp) &&
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 9;
			}
			else if (type == Enums::ActionCardType::Map)
			{
				return 5;
			}
			else if (type == Enums::ActionCardType::RockFall)
			{
				return 4;
			}
			else if (type == Enums::ActionCardType::Inspection)
			{
				return 3;
			}
			else if ((type == Enums::ActionCardType::BrokenCart ||
				type == Enums::ActionCardType::BrokenLamp ||
				type == Enums::ActionCardType::BrokenPickaxe) &&
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 2;
			}
			else if (type == Enums::ActionCardType::SwapYourHand)
			{
				return 1;
			}
			else if (type == Enums::ActionCardType::SwapYourHats)
			{
				return 0;
			}
		}
		else if (pathCard != nullptr)
		{
			const int highPriority = 8;
			const int mediumPriority = 7;
			const int lowPriority = 6;

			if (pathCard->getType() == Enums::PathCardType::NSWEC) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWEC_C) return highPriority;

			if (pathCard->getType() == Enums::PathCardType::NSC_B) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC_B) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_B) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSC_G) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC_G) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_S_G) return mediumPriority;

			if (pathCard->getType() == Enums::PathCardType::NSC) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSC_W_E) return highPriority;

			if (pathCard->getType() == Enums::PathCardType::WEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SW) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NW) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NS) return mediumPriority;

			if (pathCard->getType() == Enums::PathCardType::SEC_W) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_N_S) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_S) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NEC_SWC) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC_SEC) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC_E) return mediumPriority;

			if (pathCard->getType() == Enums::PathCardType::NEC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::WC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::SEC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::NC_T) return lowPriority;

			if (pathCard->getType() == Enums::PathCardType::WEC_NSC) return lowPriority;

			if (pathCard->getType() == Enums::PathCardType::NC_C) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::WC_C) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWC_C) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWC_E_C) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::SWEC_C) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NWEC_S_C) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSC_W_C) return highPriority;

			return 0;
		}
	}
	else
	{
		if (actionCard != nullptr)
		{
			Enums::ActionCardType type = actionCard->getType();

			if (type == Enums::ActionCardType::FreeAtLeast && player->getTrappedState())
			{
				return 12;
			}
			else if (type == Enums::ActionCardType::Theft)
			{
				return 11;
			}
			else if (type == Enums::ActionCardType::HandsOff)
			{
				return 10;
			}
			else if ((type == Enums::ActionCardType::RepairCart ||
				type == Enums::ActionCardType::RepairLamp ||
				type == Enums::ActionCardType::RepairLampAndCart ||
				type == Enums::ActionCardType::RepairPickaxe ||
				type == Enums::ActionCardType::RepairPickaxeAndCart ||
				type == Enums::ActionCardType::RepairPickaxeAndLamp) &&
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 9;
			}
			else if (type == Enums::ActionCardType::Map)
			{
				return 5;
			}
			else if (type == Enums::ActionCardType::RockFall)
			{
				return 4;
			}
			else if (type == Enums::ActionCardType::Inspection)
			{
				return 3;
			}
			else if ((type == Enums::ActionCardType::BrokenCart ||
				type == Enums::ActionCardType::BrokenLamp ||
				type == Enums::ActionCardType::BrokenPickaxe) &&
				actionCardAffectedPlayer(type, player) != nullptr)
			{
				return 2;
			}
			else if (type == Enums::ActionCardType::SwapYourHand)
			{
				return 1;
			}
			else if (type == Enums::ActionCardType::SwapYourHats)
			{
				return 0;
			}
		}
		else if (pathCard != nullptr)
		{
			const int highPriority = 8;
			const int mediumPriority = 7;
			const int lowPriority = 6;

			if (pathCard->getType() == Enums::PathCardType::NSWEC) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWEC_C) return highPriority;

			if (pathCard->getType() == Enums::PathCardType::NSC_B) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC_B) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_B) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSC_G) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC_G) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_S_G) return mediumPriority;

			if (pathCard->getType() == Enums::PathCardType::NSC) return highPriority;
			if (pathCard->getType() == Enums::PathCardType::NSC_W_E) return highPriority;

			if (pathCard->getType() == Enums::PathCardType::WEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SW) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NW) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSEC) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSE) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NS) return mediumPriority;

			if (pathCard->getType() == Enums::PathCardType::SEC_W) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_N_S) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WEC_S) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NEC_SWC) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::NWC_SEC) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::SWC_E) return mediumPriority;

			if (pathCard->getType() == Enums::PathCardType::NEC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::WC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::SEC_T) return lowPriority;
			if (pathCard->getType() == Enums::PathCardType::NC_T) return lowPriority;

			if (pathCard->getType() == Enums::PathCardType::WEC_NSC) return lowPriority;

			if (pathCard->getType() == Enums::PathCardType::NC_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::WC_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWC_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSWC_E_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::SWEC_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NWEC_S_C) return mediumPriority;
			if (pathCard->getType() == Enums::PathCardType::NSC_W_C) return mediumPriority;

			return 0;
		}
	}
	return 0;
}

Board::Position Game::getPlacingPosition(PathCard* pathCard)
{
	for (Board::Position position : pathCardsPosition)
	{
		auto northPosition = Board::Position(position.first - 1, position.second);
		auto southPosition = Board::Position(position.first + 1, position.second);
		auto westPosition = Board::Position(position.first, position.second - 1);
		auto eastPosition = Board::Position(position.first, position.second + 1);

		if (inBound(northPosition) && canPutCardOnBoard(board, northPosition, pathCard))
			return northPosition;
		else if (inBound(southPosition) && canPutCardOnBoard(board, southPosition, pathCard))
			return southPosition;
		else if (inBound(westPosition) && canPutCardOnBoard(board, westPosition, pathCard))
			return westPosition;
		else if (inBound(eastPosition) && canPutCardOnBoard(board, eastPosition, pathCard))
			return eastPosition;

		pathCard->rotate();

		if (inBound(northPosition) && canPutCardOnBoard(board, northPosition, pathCard))
			return northPosition;
		else if (inBound(southPosition) && canPutCardOnBoard(board, southPosition, pathCard))
			return southPosition;
		else if (inBound(westPosition) && canPutCardOnBoard(board, westPosition, pathCard))
			return westPosition;
		else if (inBound(eastPosition) && canPutCardOnBoard(board, eastPosition, pathCard))
			return eastPosition;
	}
	return Board::Position(-1, -1);
}

void Game::selectCardFromHand(const int& currentPlayerIndex, std::vector<int>& selectedCardIndexes)
{
	auto currentCards = this->players[currentPlayerIndex]->getCards().getDeck();
		
	int maxCardIndex = 0, count = 0;
	std::vector<std::pair<int, int>> resetIndex;
	while (true)
	{
		auto maxCardPriority = std::max_element(currentCards.begin(), currentCards.end(),
			[](std::optional<Card*> a, std::optional<Card*> b)
		{
			return a.value()->priority < b.value()->priority;
		});
		int max = std::distance(currentCards.begin(), maxCardPriority);
		maxCardIndex = max;

		PathCard* pathCard = dynamic_cast<PathCard*>(currentCards[maxCardIndex].value());
		ActionCard* actionCard = dynamic_cast<ActionCard*>(currentCards[maxCardIndex].value());
		if (actionCard != nullptr && actionCardAffectedPlayer(actionCard->getType(), this->players[currentPlayerIndex]) != nullptr)
		{
			break;
		}
		else if (pathCard != nullptr && getPlacingPosition(pathCard) != Board::Position(-1, -1) &&
			this->players[currentPlayerIndex]->getCartState() &&
			this->players[currentPlayerIndex]->getLampState() &&
			this->players[currentPlayerIndex]->getPickaxeState() &&
			!this->players[currentPlayerIndex]->getTrappedState())
		{
			break;
		}
		resetIndex.push_back(std::make_pair(maxCardIndex, currentCards[maxCardIndex].value()->priority));
		currentCards[maxCardIndex].value()->priority = 0;
		
		/* Error checker */
		if (++count >= 5)
			break;
	}

	for (std::pair<int, int> elem : resetIndex)
	{
		currentCards[elem.first].value()->priority = elem.second;
	}

	if (selectedCardIndexes.size() != 0)
		selectedCardIndexes.clear();

	selectedCardIndexes.push_back(maxCardIndex);
}

Board::Position Game::getCardToDestroy(const Enums::PlayerType& playerType)
{
	Board::Position startPosition = Board::Position(8, 2);
	for (Board::Position cardPosition : pathCardsPosition)
	{
		if (playerType == Enums::PlayerType::Saboteur &&
			this->board[cardPosition].has_value() &&
			this->board[cardPosition].value().getPathToCenter() &&
			cardPosition != startPosition)
		{
			return cardPosition;
		}
		else if (this->board[cardPosition].has_value() &&
				!this->board[cardPosition].value().getPathToCenter() &&
				cardPosition != startPosition)
		{
			return cardPosition;
		}
	}

	for (Board::Position cardPosition : pathCardsPosition)
	{
		if (this->board[cardPosition].has_value() && cardPosition != startPosition)
		{
			return cardPosition;
		}
	}
}

void Game::executeCardFromHand(const int & currentPlayerIndex, std::vector<int>& selectedCardIndexes)
{
	if (selectedCardIndexes.size() == 1)
	{
		auto currentCards = this->players[currentPlayerIndex]->getCards().getDeck();
		auto currentPlayer = this->players[currentPlayerIndex];

		PathCard* pathCard = dynamic_cast<PathCard*>(currentCards[selectedCardIndexes[0]].value());
		ActionCard* actionCard = dynamic_cast<ActionCard*>(currentCards[selectedCardIndexes[0]].value());

		if (actionCard != nullptr)
		{
			Board::Position position = Board::Position(-1, -1);

			if (actionCard->getType() == Enums::ActionCardType::BrokenCart ||
				actionCard->getType() == Enums::ActionCardType::BrokenLamp || 
				actionCard->getType() == Enums::ActionCardType::BrokenPickaxe ||
				actionCard->getType() == Enums::ActionCardType::Trapped)
			{
				Player* affectedPlayer;

				affectedPlayer = actionCardAffectedPlayer(actionCard->getType(), currentPlayer);

				actionCard->executeAction(currentPlayer, affectedPlayer, board, position);
			}
			else
			{
				position = getCardToDestroy(currentPlayer->getType());
				actionCard->executeAction(currentPlayer, currentPlayer, board, position);
			}
		}
		else if (pathCard != nullptr)
		{
			auto placingPosition = getPlacingPosition(pathCard);
			if (placingPosition != Board::Position(-1, -1) && 
				currentPlayer->getCartState() &&
				currentPlayer->getLampState() &&
				currentPlayer->getPickaxeState() &&
				!currentPlayer->getTrappedState())
				board[placingPosition] = *pathCard;
		}
	}
}

void Game::distributePathCards()
{
	pathCardDeck.shuffle();
	int index = 0;
	for (auto player : players)
	{
		distributePathCard(numberOfPlayers, player, index);
	}
}

void Game::distributeGold(const Enums::PlayerType& type)
{
	this->nrOfDwarf = 0;
	for (auto player : players)
	{
		if (player->getType() == type)
		{
			this->nrOfDwarf++;
		}
	}

	int goldNuggets = 0;
	switch (this->nrOfDwarf)
	{
	case 1:
		goldNuggets = 5;
		break;
	case 2:
		goldNuggets = 4;
		break;
	case 3:
		goldNuggets = 3;
		break;
	case 4:
		goldNuggets = 2;
		break;
	default:
		goldNuggets = 1;
		break;
	}

	if (type == Enums::PlayerType::BlueTeam || type == Enums::PlayerType::GreenTeam)
	{
		dllCall(myString, mySaboteurDLL, Saboteur::Actions::diggersWon, Saboteur::Type::Info);
		dllCall(myString, mySaboteurDLL, Saboteur::Actions::goldDistribution, Saboteur::Type::Info);

		for (auto player : players)
		{
			if (!player->getTrappedState())
			{
				if (player->getType() == type)
				{
					player->setGold(player->getGold() + goldNuggets);
				}
				else if (player->getType() == Enums::PlayerType::Boss && (goldNuggets - 1) > 0)
				{
					player->setGold(player->getGold() + (goldNuggets - 1));
				}
				else if (player->getType() == Enums::PlayerType::Profiteer && (goldNuggets - 2) > 0)
				{
					player->setGold(player->getGold() + (goldNuggets - 2));
				}
				else if (player->getType() == Enums::PlayerType::Geologist)
				{
					player->setGold(player->getGold() + this->nrOfCrystals);
				}
			}
		}
	}
	else if (type == Enums::PlayerType::Saboteur)
	{
		dllCall(myString, mySaboteurDLL, Saboteur::Actions::saboteursWon, Saboteur::Type::Info);
		dllCall(myString, mySaboteurDLL, Saboteur::Actions::goldDistribution, Saboteur::Type::Info);

		for (auto player : players)
		{
			if (!player->getTrappedState())
			{
				if (player->getType() == type)
				{
					player->setGold(player->getGold() + goldNuggets);
				}
				else if (player->getType() == Enums::PlayerType::Profiteer && (goldNuggets - 2) > 0)
				{
					player->setGold(player->getGold() + abs(goldNuggets - 2));
				}
				else if (player->getType() == Enums::PlayerType::Geologist)
				{
					player->setGold(player->getGold() + this->nrOfCrystals);
				}
			}
		}
	}

	for (Player* player : players)
	{
		if (player->getTheftState() == true)
		{
			auto it = std::max_element(players.begin(), players.end(),
				[](Player* a, Player* b)
			{
				return a->getGold() < b->getGold();
			});

			if ((*it)->getGold() > 0)
			{
				player->setGold(player->getGold() + 1);
				(*it)->setGold((*it)->getGold() - 1);
			}
		}
	}
}

void Game::verifyIfRoundIsOver(const int& currentPlayerIndex)
{
	Player* currentPlayer = players[currentPlayerIndex];

	if (board.pathReachedGold(board, currentPlayer->getType()) == true)
	{
		dllCall(myString, mySaboteurDLL, Saboteur::Actions::roundEnded, Saboteur::Type::Info);

		Enums::PlayerType winner = Enums::PlayerType::Undefinded;
		for (auto player : players)
		{
			if (player->getType() == board.winners)
			{
				winner = board.winners;
			}
		}

		if (winner == Enums::PlayerType::GreenTeam && winner != Enums::PlayerType::Undefinded)
		{
			distributeGold(Enums::PlayerType::GreenTeam);
		}
		else if (winner == Enums::PlayerType::BlueTeam && winner != Enums::PlayerType::Undefinded)
		{
			distributeGold(Enums::PlayerType::BlueTeam);
		}

		this->reset();

		this->round++;
	}
	else if (allPlayersHaveEmptyHands())
	{
		int position = -1;
		for (int i = 0; i < players.size(); i++)
		{
			if (players[i]->getType() == Enums::PlayerType::Saboteur)
				position = i;
		}

		if (position != -1)
		{
			auto it = players.begin() + position;

			distributeGold(Enums::PlayerType::Saboteur);
		}

		this->reset();

		this->round++;
	}
}


void Game::reset()
{
	this->board.InitializeBoard();
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::boardCreated, Saboteur::Type::Info);
	this->initPathCardDeck();
	this->initDwarfCardDeck();
	this->distributeDwarfCards();
	this->distributePathCards();
	this->extractTenCards();
	this->resetPlayersState();

	pathCardsPosition.push_back(Board::Position(8, 2));
}

bool Game::inBound(Board::Position position)
{
	if (position.first >= 0 && position.second >= 0)
	{
		if (position.first < Board::kHeight && position.second < Board::kWidth)
		{
			return true;
		}
	}
	return false;
}

void Game::discardCard(const int& index, const int& choiceIndex)
{
	auto& playerCards = players[index]->getCards().getDeck();
	auto begin = playerCards.begin();
	playerCards.erase(begin + choiceIndex);
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::currentPlayer, Saboteur::Type::Info);
	myString = " " + players[index]->getName();
	mySaboteurDLL.log(myString);

	if (!pathCardDeck.getDeck().empty())
	{
		playerCards.push_back(std::move(pathCardDeck[pathCardDeck.getDeck().size() - 1]));
		pathCardDeck.getDeck().erase(pathCardDeck.getDeck().end() - 1);
		playerCards[playerCards.size() - 1].value()->priority = getPriority(players[index], playerCards[playerCards.size() - 1].value());
	}
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::playerChoose, Saboteur::Type::Info);
}

int Game::getMinPlayerIndex()
{
	auto min = std::min_element(players.begin(), players.end(),
		[](Player* player1, Player* player2)
	{
		return player1->getAge() < player2->getAge();
	});

	return std::distance(players.begin(), min);
}

void Game::addCrystal()
{
	this->nrOfCrystals++;
}

void Game::resetPlayersState()
{
	for (Player* player : players)
	{
		player->setCart(true);
		player->setLamp(true);
		player->setPickaxe(true);
		player->setTheft(false);
		player->setTrapped(false);
	}
}
