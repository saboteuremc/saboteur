#pragma once

#include <vector>
#include <memory>
#include <algorithm>

#include <iterator>
#include <random>
#include <tuple>
#include <queue>

//#include "../../DLL/DLL/SaboteurV1.h"
//#include <fstream> 

#include "Board.h"
#include "Player.h"
#include "CardDeck.h"

#include "ActionCard.h"
#include "PathCard.h"

class Game
{
public:
	const bool GOLD_MINER = true;
	const bool SABOTEUR = false;
public:
	/* Constructor */
	Game();

	/* Getters */
	Board& getBoard();
	std::vector<Player*>& getPlayers();
	CardDeck& getPathCardDeck();
	CardDeck& getDiscardDeck();
	std::vector<Enums::PlayerType>& getDwarfCardDeck();
	std::vector<uint8_t>& getGoldCardDeck();
	int getNumberOfPlayers();
	int getRound();

	/* Setters */
	void setBoard(Board board);
	void setPlayers(std::vector<Player*> players);
	void setPathCardDeck(CardDeck pathCardDeck);
	void setDiscardDeck(CardDeck discardDeck);
	void setDwarfCardDeck(std::vector<Enums::PlayerType> dwarfCardDeck);
	void setGoldcardDeck(std::vector<uint8_t> goldCardDeck);
	void setNumberOfPlayers(const int& numberOfPlayers);
	void extractTenCards();

public:
	void initPathCardDeck();
	void initPathCards(int& index);
	void initActionCards(int& index);
	void initDwarfCardDeck();

	void distributePathCard(const int& numberOfPlayers, Player* player, int& index);
	void distributeDwarfCards();
	void distributePathCards();
	void distributeGold(const Enums::PlayerType& type);

	bool allPlayersHaveEmptyHands();

public:
	void discardCard(const int& index, const int& choiceIndex);
	void verifyIfRoundIsOver(const int& currentPlayeIndex);
	bool canPutCardOnBoard(Board board, Board::Position position, PathCard* pathCard);
	void initGoldCardDeck();
	int getMinPlayerIndex();
	void addCrystal();
	void resetPlayersState();
	void reset();
	bool inBound(Board::Position position);
	bool allowEntryPlacement(Board& board, Board::Position position);

	/* Opponent */
private:
	/* Return the Priority of a card influenced by the Player Type*/
	int getPriority(Player* playerType, Card* card);

	/* Return the Position where the Computer can place a card on the Board */
	Board::Position getPlacingPosition(PathCard* pathCard);

	/* Return the Position of the card that need to be Destroyed */
	Board::Position getCardToDestroy(const Enums::PlayerType& playerType);

	/* Tell the Computer Opponent if he can use the corresponding Action Card */
	Player * actionCardAffectedPlayer(const Enums::ActionCardType& type, Player * currentPlayer);

public:
	/* Select the card from the Computer hand, used to show the selected Computer hand Card */
	void selectCardFromHand(const int& currentPlayerIndex, std::vector<int>& selectedCardIndexes);

	/* Execute the Card that the Computer choose */
	void executeCardFromHand(const int& currentPlayerIndex, std::vector<int>& selectedCardIndexes);

private:
	Board board;
	std::vector<Player*> players;

	CardDeck pathCardDeck;
	CardDeck discardDeck;

	std::vector<Enums::PlayerType> dwarfCardDeck;
	std::vector<uint8_t> goldCardDeck;
	int goldCardDeckIndex = goldCardDeck.size() - 1;
	int numberOfPlayers;

	int round;
	int nrOfCrystals;
	int nrOfDwarf;

	std::vector<Board::Position> pathCardsPosition;
};