#pragma once
#include "Enums.h"
#include<iostream>
class Card
{
public:
	Card() = default;
	~Card() = default;

	void virtual Print() = 0;

	int priority;
};

