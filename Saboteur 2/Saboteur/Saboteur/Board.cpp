#include "Board.h"
#include <vector>
#include <algorithm>
#include <queue>
#include <iterator>
#include <random>
#include <functional>
#include "Graphic.h"

///
template<typename T, typename priority_t>
struct PriorityQueue {
	typedef std::pair<priority_t, T> PQElement;
	std::priority_queue<PQElement, std::vector<PQElement>,
		std::greater<PQElement>> elements;

	inline bool empty() const {
		return elements.empty();
	}

	inline void put(T item, priority_t priority) {
		elements.emplace(priority, item);
	}

	T get() {
		T best_item = elements.top().second;
		elements.pop();
		return best_item;
	}
};


Board::Board()
{
	InitializeBoard();
}

const std::optional<PathCard>& Board::operator[](const Position & position) const
{
	const auto&[line, column] = position;

	if (line >= kHeight || column >= kWidth)
		throw "Board index out of bound.";

	return m_cards[line * kWidth + column];
}


std::optional<PathCard>& Board::operator[](const Position & position)
{
	const auto&[line, column] = position;

	if (line >= kHeight || column >= kWidth)
	{
		throw "Board index out of bound.";
	}

	return m_cards[line * kWidth + column];
}

std::array<std::optional<PathCard>, Board::kSize>& Board::getCards()
{
	return this->m_cards;
}

/* Function to shuffle every round the final cards*/
void Board::InitializeBoard()
{
	for (auto& position : this->m_cards)
	{
		position = std::nullopt;
	}

	std::vector<Board::Position> finalCards;
	finalCards.resize(3);
	finalCards[0] = Board::Position(0, 0);
	finalCards[1] = Board::Position(0, 2);
	finalCards[2] = Board::Position(0, 4);

	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::shuffle(finalCards.begin(), finalCards.end(), generator);

	std::optional<PathCard> goldCard = PathCard(Enums::PathCardType::NSWEC, true);
	std::optional<PathCard> rockCard = PathCard(Enums::PathCardType::NSWEC, false);
	
	//initialize ends
	this->m_cards[finalCards[0].first*kWidth + finalCards[0].second] = goldCard;
	this->m_cards[finalCards[1].first*kWidth + finalCards[1].second] = rockCard;
	this->m_cards[finalCards[2].first*kWidth + finalCards[2].second] = rockCard;

	//initialize start 
	this->m_cards[8 * kWidth + 2] = std::optional<PathCard>(Enums::PathCardType::NSWEC);
	this->m_cards[8 * kWidth + 2].value().setIsEntry(true);
}



void Board::markEntries(Board& board)
{
	for (int i = 0; i < Board::kHeight; ++i)
	{
		for (int j = 0; j < Board::kWidth; ++j)
		{
			if (board[Board::Position(i, j)].has_value() && board[Board::Position(i, j)].value().getIfIsEntry())
			{
				entries.push_back(Board::Position(i, j));
			}
		}
	}
}

Board::Position Board::getGoldPosition()
{
	Position goldPosition;
	goldPosition.first = 0;

	if (this->m_cards[0].value().isFinalGoldCard())
	{
		goldPosition.second = 0;
		return goldPosition;
	}

	if (this->m_cards[2].value().isFinalGoldCard())
	{
		goldPosition.second = 2;
		return goldPosition;
	}

	if (this->m_cards[4].value().isFinalGoldCard())
	{
		goldPosition.second = 4;
		return goldPosition;
	}

	
}

float heuristic(const Board::Position& next, const Board::Position& goal)
{
	return std::abs(next.first - goal.first) + std::abs(next.second - goal.second);
}

bool Board::pathReachedGold(Board& board, const Enums::PlayerType& currentPlayer)
{
	std::queue<Board::Position> path;

	std::vector<Board::Position> visited;
	std::vector<std::pair<Board::Position, PathCard>> resetCoord;

	Board::Position goldPosition = board.getGoldPosition();

	PriorityQueue <Board::Position, int> frontier;

	markEntries(board);

	Enums::PlayerType currentTeam;
	if (currentPlayer == Enums::PlayerType::GreenTeam ||
		currentPlayer == Enums::PlayerType::BlueTeam)
	{
		currentTeam = currentPlayer;
	}
	else
	{
		currentTeam = Enums::PlayerType::GreenTeam;
	}

	for (int i = 0; i < 2; ++i)
	{
		if (i == 1 && currentTeam == Enums::PlayerType::GreenTeam)
		{
			currentTeam = Enums::PlayerType::BlueTeam;
		}
		else if (i == 1 && currentTeam == Enums::PlayerType::BlueTeam)
		{
			currentTeam = Enums::PlayerType::GreenTeam;
		}

		for (Board::Position entry : entries)
		{
			frontier.put(entry, 0);

			visited.push_back(entry);
			while (!frontier.empty())
			{
				auto front = frontier.get();
				if (board[front].value().isFinalGoldCard())
				{
					this->winners = currentTeam;
					entries.clear();
					return true;
				}

				//west
				auto westPosition = Board::Position(front.first, front.second - 1);
				if (inBound(westPosition))
				{
					if ((board[westPosition].has_value() &&
						board[front].value().getPathToWest() &&
						board[westPosition].value().getPathToEast() &&
						board[front].value().getPathToCenter() &&
						std::find(visited.begin(), visited.end(), westPosition) == visited.end())
						||
						(board[westPosition].has_value() &&
							board[westPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
							std::find(visited.begin(), visited.end(), westPosition) == visited.end())
						||
						(board[westPosition].has_value() &&
							board[westPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
							std::find(visited.begin(), visited.end(), westPosition) == visited.end())
						||
						(board[westPosition].has_value() &&
							board[westPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
							std::find(visited.begin(), visited.end(), westPosition) == visited.end()))
					{
						if (board[westPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
							board[westPosition].value().getVisitedWEC() == false)
						{
							board[westPosition].value().setPossiblePaths(Enums::PathCardType::WEC);
							board[westPosition].value().setVisitedWEC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(westPosition, PathCard(Enums::PathCardType::WEC_NSC, false)));

							///
							int priority = heuristic(westPosition, goldPosition);
							frontier.put(westPosition, priority);
						}
						else if (board[westPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
							board[westPosition].value().getVisitedSEC() == false)
						{
							board[westPosition].value().setPossiblePaths(Enums::PathCardType::NWC);
							board[westPosition].value().rotate();
							board[westPosition].value().setVisitedSEC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(westPosition, PathCard(Enums::PathCardType::NWC_SEC, false)));

							///
							int priority = heuristic(westPosition, goldPosition);
							frontier.put(westPosition, priority);
						}
						else if (board[westPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
							board[westPosition].value().getVisitedNEC() == false)
						{
							board[westPosition].value().setPossiblePaths(Enums::PathCardType::SWC);
							board[westPosition].value().rotate();
							board[westPosition].value().setVisitedNEC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(westPosition, PathCard(Enums::PathCardType::NEC_SWC, false)));

							///
							int priority = heuristic(westPosition, goldPosition);
							frontier.put(westPosition, priority);
						}
						else if (board[westPosition].value().getType() != Enums::PathCardType::WEC_NSC &&
							board[westPosition].value().getType() != Enums::PathCardType::NWC_SEC &&
							board[westPosition].value().getType() != Enums::PathCardType::NEC_SWC)
						{
							///
							int priority = heuristic(westPosition, goldPosition);
							frontier.put(westPosition, priority);
							visited.push_back(westPosition);
						}
					}
				}

				//south
				auto southPosition = Board::Position(front.first + 1, front.second);
				if (inBound(southPosition))
				{
					if ((board[southPosition].has_value() &&
						board[front].value().getPathToSouth() &&
						board[front].value().getPathToCenter() &&
						board[southPosition].value().getPathToNorth() &&
						std::find(visited.begin(), visited.end(), southPosition) == visited.end())
						||
						(board[southPosition].has_value() &&
							board[southPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
							std::find(visited.begin(), visited.end(), southPosition) == visited.end())
						||
						(board[southPosition].has_value() &&
							board[southPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
							std::find(visited.begin(), visited.end(), southPosition) == visited.end())
						||
						(board[southPosition].has_value() &&
							board[southPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
							std::find(visited.begin(), visited.end(), southPosition) == visited.end()))
					{
						if (board[southPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
							board[southPosition].value().getVisitedNSC() == false)
						{
							board[southPosition].value().setPossiblePaths(Enums::PathCardType::NSC);
							board[southPosition].value().setVisitedNSC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(southPosition, PathCard(Enums::PathCardType::WEC_NSC, false)));

							///
							int priority = heuristic(southPosition, goldPosition);
							frontier.put(southPosition, priority);
						}
						else if (board[southPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
							board[southPosition].value().getVisitedNWC() == false)
						{
							board[southPosition].value().setPossiblePaths(Enums::PathCardType::NWC);
							board[southPosition].value().setVisitedNWC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(southPosition, PathCard(Enums::PathCardType::NWC_SEC, false)));

							///
							int priority = heuristic(southPosition, goldPosition);
							frontier.put(southPosition, priority);
						}
						else if (board[southPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
							board[southPosition].value().getVisitedNEC() == false)
						{
							board[southPosition].value().setPossiblePaths(Enums::PathCardType::SWC);
							board[southPosition].value().rotate();
							board[southPosition].value().setVisitedNEC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(southPosition, PathCard(Enums::PathCardType::NEC_SWC, false)));

							///
							int priority = heuristic(southPosition, goldPosition);
							frontier.put(southPosition, priority);
						}
						else if (board[southPosition].value().getType() != Enums::PathCardType::WEC_NSC &&
							board[southPosition].value().getType() != Enums::PathCardType::NWC_SEC &&
							board[southPosition].value().getType() != Enums::PathCardType::NEC_SWC)
						{

							///
							int priority = heuristic(southPosition, goldPosition);
							frontier.put(southPosition, priority);
							visited.push_back(southPosition);
						}
					}
				}

				//east
				auto eastPosition = Board::Position(front.first, front.second + 1);
				if (inBound(eastPosition))
				{
					if ((board[eastPosition].has_value() &&
						board[front].value().getPathToEast() &&
						board[front].value().getPathToCenter() &&
						board[eastPosition].value().getPathToWest() &&
						std::find(visited.begin(), visited.end(), eastPosition) == visited.end())
						||
						(board[eastPosition].has_value() &&
							board[eastPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
							std::find(visited.begin(), visited.end(), eastPosition) == visited.end())
						||
						(board[eastPosition].has_value() &&
							board[eastPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
							std::find(visited.begin(), visited.end(), eastPosition) == visited.end())
						||
						(board[eastPosition].has_value() &&
							board[eastPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
							std::find(visited.begin(), visited.end(), eastPosition) == visited.end()))
					{
						if (board[eastPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
							board[eastPosition].value().getVisitedWEC() == false)
						{
							board[eastPosition].value().setPossiblePaths(Enums::PathCardType::WEC);
							board[eastPosition].value().setVisitedWEC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(eastPosition, PathCard(Enums::PathCardType::WEC_NSC, false)));

							///
							path.push(eastPosition);
						}
						else if (board[eastPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
							board[eastPosition].value().getVisitedNWC() == false)
						{
							board[eastPosition].value().setPossiblePaths(Enums::PathCardType::NWC);
							board[eastPosition].value().setVisitedNWC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(eastPosition, PathCard(Enums::PathCardType::NWC_SEC, false)));

							///
							int priority = heuristic(eastPosition, goldPosition);
							frontier.put(eastPosition, priority);
						}
						else if (board[eastPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
							board[eastPosition].value().getVisitedSWC() == false)
						{
							board[eastPosition].value().setPossiblePaths(Enums::PathCardType::SWC);
							board[eastPosition].value().setVisitedSWC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(eastPosition, PathCard(Enums::PathCardType::NEC_SWC, false)));

							///
							int priority = heuristic(eastPosition, goldPosition);
							frontier.put(eastPosition, priority);
						}
						else if (board[eastPosition].value().getType() != Enums::PathCardType::WEC_NSC &&
							board[eastPosition].value().getType() != Enums::PathCardType::NWC_SEC &&
							board[eastPosition].value().getType() != Enums::PathCardType::NEC_SWC)
						{

							///
							int priority = heuristic(eastPosition, goldPosition);
							frontier.put(eastPosition, priority);
							visited.push_back(eastPosition);
						}
					}
				}

				//north
				auto northPosition = Board::Position(front.first - 1, front.second);
				if (inBound(northPosition))
				{
					if ((board[northPosition] &&
						board[front].value().getPathToNorth() &&
						board[front].value().getPathToCenter() &&
						board[northPosition].value().getPathToSouth() &&
						std::find(visited.begin(), visited.end(), northPosition) == visited.end())
						||
						(board[northPosition].has_value() &&
							board[northPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
							std::find(visited.begin(), visited.end(), northPosition) == visited.end())
						||
						(board[northPosition].has_value() &&
							board[northPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
							std::find(visited.begin(), visited.end(), northPosition) == visited.end())
						||
						(board[northPosition].has_value() &&
							board[northPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
							std::find(visited.begin(), visited.end(), northPosition) == visited.end()))
					{
						if (board[northPosition].value().getType() == Enums::PathCardType::WEC_NSC &&
							board[northPosition].value().getVisitedNSC() == false)
						{
							board[northPosition].value().setPossiblePaths(Enums::PathCardType::NSC);
							board[northPosition].value().setVisitedNSC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(northPosition, PathCard(Enums::PathCardType::WEC_NSC, false)));


							///
							int priority = heuristic(northPosition, goldPosition);
							frontier.put(northPosition, priority);
						}
						else if (board[northPosition].value().getType() == Enums::PathCardType::NWC_SEC &&
							board[northPosition].value().getVisitedSEC() == false)
						{
							board[northPosition].value().setPossiblePaths(Enums::PathCardType::NWC);
							board[northPosition].value().rotate();
							board[northPosition].value().setVisitedSEC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(northPosition, PathCard(Enums::PathCardType::NWC_SEC, false)));


							///
							int priority = heuristic(northPosition, goldPosition);
							frontier.put(northPosition, priority);
						}
						else if (board[northPosition].value().getType() == Enums::PathCardType::NEC_SWC &&
							board[northPosition].value().getVisitedSWC() == false)
						{
							board[northPosition].value().setPossiblePaths(Enums::PathCardType::SWC);
							board[northPosition].value().setVisitedSWC(true);

							resetCoord.push_back(std::pair<Board::Position, PathCard>(northPosition, PathCard(Enums::PathCardType::NEC_SWC, false)));

							///
							int priority = heuristic(northPosition, goldPosition);
							frontier.put(northPosition, priority);
						}
						else if (board[northPosition].value().getType() != Enums::PathCardType::WEC_NSC &&
							board[northPosition].value().getType() != Enums::PathCardType::NWC_SEC &&
							board[northPosition].value().getType() != Enums::PathCardType::NEC_SWC)
						{

							///
							int priority = heuristic(northPosition, goldPosition);
							frontier.put(northPosition, priority);
							visited.push_back(northPosition);
						}
					}
				}
				///frontier.pop();
			}

			for (std::pair<Board::Position, PathCard> card : resetCoord)
			{
				board[card.first] = card.second;
			}
			resetCoord.clear();
		}
	}
	entries.clear();
	return false;
}

bool Board::inBound(Board::Position position)
{
	if (position.first >= 0 && position.second >= 0)
	{
		if (position.first < kHeight && position.second < kWidth)
		{
			return true;
		}
	}
	return false;
}
