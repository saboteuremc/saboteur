#include "ActionCard.h"

ActionCard::ActionCard(const Enums::ActionCardType& type) :
	type(type)
{
}

ActionCard::ActionCard(ActionCard && otherCard)
{
	*this = std::move(otherCard);
}

ActionCard::ActionCard(const ActionCard & otherCard)
{
	*this = otherCard;
}

Enums::ActionCardType ActionCard::getType()
{
	return this->type;
}

void ActionCard::setType(Enums::ActionCardType type)
{
	this->type = type;
}

ActionCard & ActionCard::operator=(const ActionCard & otherCard)
{
	this->type = otherCard.type;
	return *this;
}

ActionCard & ActionCard::operator=(ActionCard && otherCard)
{
	*(this) = otherCard;
	new(&otherCard) ActionCard;
	return *this;
}


void ActionCard::Print()
{
}

void ActionCard::executeAction(Player* playerSender, Player* playerAffected, Board & board, Board::Position & position)
{
	if (type == Enums::ActionCardType::BrokenCart ||
		type == Enums::ActionCardType::BrokenLamp ||
		type == Enums::ActionCardType::BrokenPickaxe)
	{
		executeBrokenCard(playerAffected, board, position);
	}
	else if (type == Enums::ActionCardType::RepairCart ||
		type == Enums::ActionCardType::RepairLamp ||
		type == Enums::ActionCardType::RepairLampAndCart ||
		type == Enums::ActionCardType::RepairPickaxe ||
		type == Enums::ActionCardType::RepairPickaxeAndCart ||
		type == Enums::ActionCardType::RepairPickaxeAndLamp)
	{
		executeRepairCard(playerAffected, board, position);
	}
	else if (type == Enums::ActionCardType::Map)
	{
		executeMapCard(playerAffected, board, position);
	}
	else if (type == Enums::ActionCardType::RockFall)
	{
		executeRockFallCard(playerAffected, board, position);
	}
	/* Expansion */
	else if (type == Enums::ActionCardType::Theft)
	{
		executeTheft(playerAffected);
	}
	else if (type == Enums::ActionCardType::HandsOff)
	{
		executeHandsOff(playerAffected);
	}
	else if (type == Enums::ActionCardType::SwapYourHand)
	{
		executeSwapYourHand(playerSender, playerAffected);
	}
	else if (type == Enums::ActionCardType::Inspection)
	{
		// TODO
	}
	else if (type == Enums::ActionCardType::SwapYourHats)
	{
		executeSwapYourHats(playerSender, playerAffected);
	}
	else if (type == Enums::ActionCardType::Trapped)
	{
		executeTrapped(playerAffected);
	}
	else if (type == Enums::ActionCardType::FreeAtLeast)
	{
		executeFreeAtLeast(playerAffected);
	}
}

void ActionCard::executeBrokenCard(Player* player, Board& board, Board::Position& position)
{
	position = Board::Position(-1, -1);
	switch (type)
	{
	case Enums::ActionCardType::BrokenCart:
	{
		if ((player->getCartState()))
		{
			player->setCart(false);
			position = Board::Position(0, 0);
		}

		break;
	}

	case Enums::ActionCardType::BrokenLamp:
	{
		if ((player->getLampState()))
		{
			player->setLamp(false);
			position = Board::Position(0, 0);
		}
		break;
	}

	case Enums::ActionCardType::BrokenPickaxe:
	{
		if (player->getPickaxeState())
		{
			player->setPickaxe(false);
			position = Board::Position(0, 0);
		}
		break;
	}

	default:
		break;
	}
}

void ActionCard::executeRepairCard(Player * player, Board & board, Board::Position & position)
{
	position = Board::Position(-1, -1);
	if (!player->getLampState() && this->getType() == Enums::ActionCardType::RepairLamp)
	{
		player->setLamp(true);
		position = Board::Position(0, 0);
	}
	if (!player->getPickaxeState() && this->getType() == Enums::ActionCardType::RepairPickaxe)
	{
		player->setPickaxe(true);
		position = Board::Position(0, 0);
	}
	if (!player->getCartState() && this->getType() == Enums::ActionCardType::RepairCart)
	{
		player->setCart(true);
		position = Board::Position(0, 0);
	}
	if ((!player->getLampState() || !player->getCartState()) && this->getType() == Enums::ActionCardType::RepairLampAndCart)
	{
		if (!player->getLampState())
			player->setLamp(true);
		else
			player->setCart(true);
		position = Board::Position(0, 0);
	}
	if ((!player->getPickaxeState() || !player->getCartState()) && this->getType() == Enums::ActionCardType::RepairPickaxeAndCart)
	{
		if (!player->getPickaxeState())
			player->setPickaxe(true);
		else
			player->setCart(true);
		position = Board::Position(0, 0);
	}
	if ((!player->getPickaxeState() || !player->getLampState()) && this->getType() == Enums::ActionCardType::RepairPickaxeAndLamp)
	{
		if (!player->getPickaxeState())
			player->setPickaxe(true);
		else
			player->setLamp(true);
		position = Board::Position(0, 0);
	}
}

void ActionCard::executeMapCard(Player * player, Board & board, Board::Position & position)
{
	revealDestination(position, player, board);
}

void ActionCard::executeRockFallCard(Player * player, Board & board, Board::Position & position)
{
	if (board.inBound(position))
	{
		removePath(position, board);
	}
	else
	{
		position = Board::Position(-1, -1);
	}
}

void ActionCard::executeTheft(Player * player)
{
	player->setTheft(true);
}

void ActionCard::executeHandsOff(Player * player)
{
	player->setTheft(false);
}

void ActionCard::executeSwapYourHand(Player * playerSender, Player * playerAffected)
{
	std::swap(playerSender->getCards(), playerAffected->getCards());
}

void ActionCard::executeSwapYourHats(Player * playerSender, Player * playerAffected)
{
	Enums::PlayerType type = playerSender->getType();
	playerSender->setType(playerAffected->getType());
	playerAffected->setType(type);
}

void ActionCard::executeTrapped(Player * player)
{
	player->setTrapped(true);
}

void ActionCard::executeFreeAtLeast(Player * player)
{
	player->setTrapped(false);
}

void ActionCard::revealDestination(Board::Position & position, const Player * player, Board & board)
{
	if (position.first == 0 && position.second == 0)
		position = Board::Position(0, 0);
	else if (position.first == 0 && position.second == 2)
		position = Board::Position(0, 2);
	else if (position.first == 0 && position.second == 4)
		position = Board::Position(0, 4);
	else
		position = Board::Position(-1, -1);
}

void ActionCard::removePath(Board::Position & position, Board & board)
{
	if (board[position].has_value())
	{
		board[position].reset();
		board[position] = std::nullopt;

		position = Board::Position(0, 0);
	}
}

std::istream & operator>>(std::istream & is, ActionCard & card)
{
	int type;
	is >> type;

	card.setType(static_cast<Enums::ActionCardType>(type));

	return is;
}

std::ostream & operator<<(std::ostream & os, const ActionCard & card)
{
	os << static_cast<int>(card.type);
	return os;
}
