#pragma once
#include<array>
#include<optional>
#include <vector>
#include "PathCard.h"
class Board
{
public:
	static const size_t kWidth = 5;
	static const size_t kHeight = 9;
	static const size_t kSize = kWidth * kHeight;

public:
	using Position = std::pair<uint8_t, uint8_t>;
public:
	Board() ;
	
	//Getter
	const std::optional<PathCard>& operator[] (const Position& pos) const;

	//Getter or/and Setter
	std::optional<PathCard>& operator[] (const Position& pos);
	std::array<std::optional<PathCard>, kSize>& getCards();

	void InitializeBoard();

public:
	 bool pathReachedGold(Board& board, const Enums::PlayerType& currentPlayer);
	 bool inBound(Board::Position position);

	 Enums::PlayerType winners = Enums::PlayerType::Undefinded;

private:
	void markEntries(Board& board);

	Position getGoldPosition();

private:
	std::array<std::optional<PathCard>, kSize> m_cards;
	std::vector<Board::Position> entries;
};

