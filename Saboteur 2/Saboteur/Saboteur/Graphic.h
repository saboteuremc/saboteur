#pragma once
#include <iostream>
#include <optional>
#include <array>
#include <vector>
#include <algorithm>

#include "Game.h"

#include <SFML\Graphics.hpp>

/* Graphic class will be responsible for all the visual aspects of the program
	and the connection between Backend and Interface */
class Graphic
{
public:
	Graphic();

private:
	/* SFML - Useful Functions */
	template<class shape>
	bool isMouseWithinShape(shape& sprite, sf::RenderWindow& window);
	void setPathTextureIndex(Enums::PathCardType type, float& row, float& col);
	void setActionTextureIndex(ActionCard* actionCard, float& row, float& col);
	void setPlayerTypeTextureIndex(Enums::PlayerType type, float& row, float& col);

	/* Start Window */
	void displayStartWindow();
	void eventHandlerStartWindow(sf::RenderWindow& window);
	void renderStartWindow(sf::RenderWindow& window);

	/* Game Window */
	sf::VideoMode initializeGameWindow();
	void displayGameWindow();
	void eventHandlerGameWindow(sf::RenderWindow& window);
	void renderBoard(sf::RenderWindow & window, sf::Texture& texture);
	void renderStatus(sf::RenderWindow & window);

public:
	void draw();

private:
	Game game;

	/* Sizes and Dimensions */
	const int width = 600;
	const int height = 480;
	const float scale = 4;
	const float cardTextureWidth = 246;
	const float cardTextureHeight = 352;
	const float cardWidth = cardTextureWidth / scale;
	const float cardHeight = cardTextureHeight / scale;
	const int nrOfRows = 9;
	const int nrOfColumns = 5;
	float horizontalGap;
	float verticalGap;

	/* Helper variables */
	sf::Texture primaryCardsTexture;
	sf::Texture logoTexture;
	sf::Texture backgroundTexture;
	sf::RectangleShape cardTemplate;

	/* Game Cards */
	std::vector<sf::RectangleShape> boardCards;
	std::vector<sf::RectangleShape> currentPlayerCards;

	/* All Player Stats */
	int numberOfPlayers;
	std::vector<sf::RectangleShape> allPlayersStatus;
	std::vector<sf::Text> allPlayersName;
	std::vector<sf::Text> allPlayersGold;

	/* Current Player Stats */
	int currentPlayerIndex;
	std::string currentPlayerName;
	std::string currentPlayerAge;
	sf::RectangleShape playerType;

	//int selectedCardIndex;
	std::vector<int> selectedCardIndexes;

	bool exitByForce;
	bool extractCardFromHand;

	/* Fonts */
	sf::Font arialFont;

	/* Rotate and Pass buttons */
	sf::Text rotateButton;
	sf::Text passButton;

	/* Helpers for Map Card */
	int revealCardCountDown = -1;
	sf::RectangleShape revealCard;

	/* Helpers for Action Card */
	int investigationCountDown = -1;
	int affectedPlayerIndex = -1;
	std::vector<sf::RectangleShape> affectedPlayerCards;

	/* Helper for Computer AI */
	int computerTurnCountDown = -1;
	bool isComputerTurn = false;
};

template<class shape>
inline bool Graphic::isMouseWithinShape(shape& sprite, sf::RenderWindow & window)
{
	/* Mouse position relative to the window */
	auto mouse_pos = sf::Mouse::getPosition(window);

	/* Mouse position translated into world coordinates */
	auto translated_pos = window.mapPixelToCoords(mouse_pos);

	/* Check if the Mouse was pressed the corresponding shape */
	if (sprite.getGlobalBounds().contains(translated_pos))
	{
		/* Mouse is inside the shape */
		return true;
	}
	return false;
}

