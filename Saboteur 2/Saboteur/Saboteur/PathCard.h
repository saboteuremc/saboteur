#pragma once
#include "Card.h"
#include <tuple>

class PathCard : public Card
{
public:
	using Coordonates = std::tuple<bool, bool, bool, bool>;

	/* Constructors */
	PathCard(const Enums::PathCardType& type = Enums::PathCardType::Undefined, const bool& isGold=false);
	PathCard(PathCard&& otherCard);
	PathCard(const PathCard& otherCard);

	/* Getter */
	Enums::PathCardType getType();
	bool getPathToNorth();
	bool getPathToSouth();
	bool getPathToWest();
	bool getPathToEast();
	bool getPathToCenter();
	bool isFinalGoldCard();
	bool isRotated();
	bool getHaveCrystal();
	bool getIfIsEntry();

	/* Expansion Exceptions */
	bool getVisitedNSC();
	bool getVisitedWEC();

	bool getVisitedNWC();
	bool getVisitedSEC();

	bool getVisitedNEC();
	bool getVisitedSWC();
	
	Enums::PlayerType getTeamDoor();
	Coordonates getCanConnectTo();

	/* Setter */
	void setType(Enums::PathCardType type);
	void setPathToNorth(const bool& isPath);
	void setPathToSouth(const bool& isPath);
	void setPathToWest(const bool& isPath);
	void setPathToEast(const bool& isPath);
	void setPathToCenter(const bool& isPath);
	void setToGold(const bool& isGold);
	void setRotate(const bool& rotate);
	void setIsEntry(const bool& isEntry);

	/* Expansion Exceptions */
	void setVisitedNSC(const bool& NSC);
	void setVisitedWEC(const bool& WEC);

	void setVisitedNWC(const bool& NWC);
	void setVisitedSEC(const bool& SEC);

	void setVisitedNEC(const bool& NEC);
	void setVisitedSWC(const bool& SWC);

	/* Operators */
	PathCard& operator=(const PathCard& otherCard);
	PathCard& operator=(PathCard&& otherCard);

	/* Useful Functions */
	void setPossiblePaths(const Enums::PathCardType& type);
	void setPossibleConnections(const Enums::PathCardType& type);
	void setIfHaveCrystal(const Enums::PathCardType & type);
	void setIfIsEntry(const Enums::PathCardType& type);
	void rotate();
	void Print() override;

	/* Destructor */
	~PathCard() = default;

	bool isWEC_NSC = false;

private:
	Enums::PathCardType type;
	bool pathToNorth;
	bool pathToSouth;
	bool pathToWest;
	bool pathToEast;
	bool pathToCenter;
	bool isGold;
	bool isRotate;

	Coordonates canConnectTo;
	bool haveCrystal;
	bool isAnEntry;

	/* Exceptions */
	bool visitedNSC= false;
	bool visitedWEC= false;

	bool visitedNWC= false;
	bool visitedSEC= false;

	bool visitedNEC= false;
	bool visitedSWC= false;
};

