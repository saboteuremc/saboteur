#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "Enums.h"
#include "Card.h"
#include "CardDeck.h"
#include "PathCard.h"

#include <optional>

class Player
{
public:
	/* Constructors and Destructor */
	Player(const std::string& name = "Undefined", const int& age = -1, const Enums::PlayerType& type = Enums::PlayerType::Undefinded, bool const& haveTheft = false, bool const& isTrapped = false);
	Player(const Player& other);

	~Player() = default;

	/* Getters */
	std::string getName();
	int getAge();
	uint8_t getGold();
	Enums::PlayerType getType();
	CardDeck& getCards();
	bool getPickaxeState() const;
	bool getCartState() const;
	bool getLampState() const;
	bool getTheftState() const;
	bool getTrappedState() const;
	bool getIsComputer() const;
	
	/* Setters */
	void setName(const std::string& name);
	void setAge(const int& age);
	void setType(const Enums::PlayerType& type);
	void setPickaxe(const bool& state);
	void setCart(const bool& state);
	void setLamp(const bool& state);
	void setGold(const uint8_t& gold);
	void setTheft(const bool& state);
	void setTrapped(const bool& state);
	void setIsComputer(const bool& state);

	/* Operators */
	Player& operator =(const Player& other);

private:
	std::string name;
	int age;

	uint8_t gold;
	Enums::PlayerType type;
	CardDeck cards;
	bool intactPickAxe;
	bool intactCart;
	bool intactLamp;

	/* Expansion */
	bool haveTheft;
	bool isTrapped;

	bool isComputer;
};

